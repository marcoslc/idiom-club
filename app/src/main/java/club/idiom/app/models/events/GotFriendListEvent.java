package club.idiom.app.models.events;

import java.util.List;

import club.idiom.app.models.User;

public class GotFriendListEvent {
    public List<User> users;

    public GotFriendListEvent(List<User> users) {
        this.users = users;
    }
}