package club.idiom.app.models.chatmodel;

/**
 * Created by victorraft on 07/03/16.
 */
public enum ChatStateEnum {
    unknown(-1),
    sending(0),
    sent(1),
    sentAndReceived(2),
    read(3),
    failed(4);

    private final int value;

    ChatStateEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}