package club.idiom.app.models.events;

public class AppStateEvent {

    public static class OnAppBackgroundedEvent {

    }

    public static class OnAppForegroundedEvent {

    }
}