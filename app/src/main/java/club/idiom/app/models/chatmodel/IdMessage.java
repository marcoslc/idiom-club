package club.idiom.app.models.chatmodel;

import org.jsoup.select.Evaluator;

import java.util.Date;
import java.util.Random;

/**
 * Created by marcoscardoso on 02/06/16.
 */
public class IdMessage {

    int id;
    String message;

    public IdMessage(String message) {
        this.message = message;
        this.id = new Random((new Date()).getTime()).nextInt();
    }

    public String getMessage() {
        return message;
    }
}
