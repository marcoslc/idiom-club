package club.idiom.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by victorraft on 09/03/16.
 */
public class UserIdioms implements Parcelable {
    public List<UserIdiom> learn;
    public List<UserIdiom> teach;

    public UserIdioms() {
        learn = new ArrayList<>();
        teach = new ArrayList<>();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(learn);
        dest.writeTypedList(teach);
    }

    protected UserIdioms(Parcel in) {
        this.learn = in.createTypedArrayList(UserIdiom.CREATOR);
        this.teach = in.createTypedArrayList(UserIdiom.CREATOR);
    }

    public static final Parcelable.Creator<UserIdioms> CREATOR = new Parcelable.Creator<UserIdioms>() {
        public UserIdioms createFromParcel(Parcel source) {
            return new UserIdioms(source);
        }

        public UserIdioms[] newArray(int size) {
            return new UserIdioms[size];
        }
    };


    public boolean learnsIdiom(IdiomsEnum idiomsEnum){
        for(UserIdiom idiom : learn){
            if(idiom.id == idiomsEnum.getValue())
                return true;
        }

        return false;
    }

    public boolean teachesIdiom(IdiomsEnum idiomsEnum){
        for(UserIdiom idiom : teach){
            if(idiom.id == idiomsEnum.getValue())
                return true;
        }

        return false;
    }
}