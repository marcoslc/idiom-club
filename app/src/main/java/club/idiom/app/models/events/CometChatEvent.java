package club.idiom.app.models.events;

import org.json.JSONObject;

/**
 * Created by Victor on 27/04/2016.
 */
public class CometChatEvent {
    public final JSONObject jsonObject;

    public CometChatEvent(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public static class GotOnlineListEvent extends CometChatEvent {
        public GotOnlineListEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class OnErrorEvent extends CometChatEvent {
        public OnErrorEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class OnMessageReceivedEvent extends CometChatEvent {
        public OnMessageReceivedEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class OnActionReceivedEvent extends CometChatEvent {
        public OnActionReceivedEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class GotProfileInfoEvent extends CometChatEvent {
        public GotProfileInfoEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class GotAnnouncementEvent extends CometChatEvent {
        public GotAnnouncementEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class OnAVChatMessageReceivedEvent extends CometChatEvent {
        public OnAVChatMessageReceivedEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    public static class OnActionMessageReceivedEvent extends CometChatEvent {
        public OnActionMessageReceivedEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }

    // -- Utility

    public static class OnMessageSentEvent extends CometChatEvent {
        public OnMessageSentEvent(JSONObject jsonObject) {
            super(jsonObject);
        }
    }
}