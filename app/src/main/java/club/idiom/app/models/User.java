package club.idiom.app.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

/**
 * Created by marcoscardoso on 02/03/16.
 */
public class User implements Parcelable {
    public int id;
    public String email;
    public String password;
    public String name;
    public String facebook;
    public long born;
    public String sex;
    public float tml;
    public String token;
    public boolean first_login;
    public UserIdioms idioms;
    public String status;
    public long lastactivity;
    public String photo;
    public boolean following;
    public String location;
    public String timezone;
    public String push_channel;

    public User() {
        idioms = new UserIdioms();
    }

    public String getAvatarUrl() {
        if (!TextUtils.isEmpty(photo)) {
            return photo;
        } else {
//            return TextUtils.isEmpty(facebook) ? "https://d1qb2nb5cznatu.cloudfront.net/users/1116512-medium_jpg?1427743868" : "http://graph.facebook.com/" + facebook + "/picture?type=large";
            return TextUtils.isEmpty(facebook) ? "" : "http://graph.facebook.com/" + facebook + "/picture?type=large";
        }
    }

    public User(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public boolean needRegisterIdiom() {
        return (idioms == null || idioms.learn == null || idioms.teach == null || idioms.learn.size() == 0 || idioms.teach.size() == 0);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.name);
        dest.writeString(this.facebook);
        dest.writeLong(this.born);
        dest.writeString(this.sex);
        dest.writeFloat(this.tml);
        dest.writeString(this.token);
        dest.writeByte(this.first_login ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.idioms, flags);
        dest.writeString(this.status);
        dest.writeLong(this.lastactivity);
        dest.writeString(this.photo);
        dest.writeByte(this.following ? (byte) 1 : (byte) 0);
        dest.writeString(this.location);
        dest.writeString(this.timezone);
        dest.writeString(this.push_channel);
    }

    protected User(Parcel in) {
        this.id = in.readInt();
        this.email = in.readString();
        this.password = in.readString();
        this.name = in.readString();
        this.facebook = in.readString();
        this.born = in.readLong();
        this.sex = in.readString();
        this.tml = in.readFloat();
        this.token = in.readString();
        this.first_login = in.readByte() != 0;
        this.idioms = in.readParcelable(UserIdioms.class.getClassLoader());
        this.status = in.readString();
        this.lastactivity = in.readLong();
        this.photo = in.readString();
        this.following = in.readByte() != 0;
        this.location = in.readString();
        this.timezone = in.readString();
        this.push_channel = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}