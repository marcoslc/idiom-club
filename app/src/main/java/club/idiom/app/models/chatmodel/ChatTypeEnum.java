package club.idiom.app.models.chatmodel;

/**
 * Created by victorraft on 07/03/16.
 */
public enum ChatTypeEnum {
    text(10),
    image(12),
    sound(16),
    video(14),
    file(17),
    accepted_call(31),
    receiving_call(32),
    finish_call(34),
    rejected_call(35),
    cancelled_call(36),
    timeout_call(37),
    destinatary_busy_call(38),
    unknown(4);

    private final int value;

    ChatTypeEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ChatTypeEnum getTypeForInt(int typeCode) {
        for (ChatTypeEnum type : ChatTypeEnum.values()) {
            if (type.getValue() == typeCode) {
                return type;
            }
        }

        return unknown;
    }
}