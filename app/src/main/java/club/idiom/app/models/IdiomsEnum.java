package club.idiom.app.models;

import android.content.Context;

import club.idiom.app.R;

/**
 * Created by victorraft on 07/03/16.
 */
public enum IdiomsEnum {
    unknown(0),
    pt_BR(1),
    en_US(2),
    es_ES(3);

    private final int value;

    IdiomsEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static IdiomsEnum getIdiomForInt(int idiomCode) {
        for (IdiomsEnum idiom : IdiomsEnum.values()) {
            if (idiom.getValue() == idiomCode) {
                return idiom;
            }
        }

        return unknown;
    }

    public String getIdiomName(Context ctx) {
        switch (this) {
            case pt_BR:
                return ctx.getString(R.string.idiom_ptbr);
            case en_US:
                return ctx.getString(R.string.idiom_enus);
            case es_ES:
                return ctx.getString(R.string.idiom_eses);

            default:
                return ctx.getString(R.string.idiom_unknown);
        }
    }

    public int getIdiomFlag() {
        switch (this) {
            case pt_BR:
                return R.drawable.flag_br;
            case en_US:
                return R.drawable.flag_us;
            case es_ES:
                return R.drawable.flag_es;

            default:
                return  R.mipmap.ic_launcher;
        }
    }
}