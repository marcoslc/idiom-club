package club.idiom.app.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by victorraft on 09/03/16.
 */
public class UserIdiom implements Parcelable {
    public int id;
    public String idiom;
    public List<String> tags;

    public UserIdiom(int id, String idiom) {
        this.id = id;
        this.idiom = idiom;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.idiom);
        dest.writeStringList(this.tags);
    }

    public UserIdiom() {
    }

    protected UserIdiom(Parcel in) {
        this.id = in.readInt();
        this.idiom = in.readString();
        this.tags = in.createStringArrayList();
    }

    public static final Parcelable.Creator<UserIdiom> CREATOR = new Parcelable.Creator<UserIdiom>() {
        public UserIdiom createFromParcel(Parcel source) {
            return new UserIdiom(source);
        }

        public UserIdiom[] newArray(int size) {
            return new UserIdiom[size];
        }
    };
}
