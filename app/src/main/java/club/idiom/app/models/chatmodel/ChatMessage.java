package club.idiom.app.models.chatmodel;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.Date;

import club.idiom.app.api.ApiFactory;

import static club.idiom.app.utils.PushNotificationManager.PushNotificationKeys.ALERT;
import static club.idiom.app.utils.PushNotificationManager.PushNotificationKeys.CALL_ID;
import static club.idiom.app.utils.PushNotificationManager.PushNotificationKeys.FROM_ID;
import static club.idiom.app.utils.PushNotificationManager.PushNotificationKeys.ID;
import static club.idiom.app.utils.PushNotificationManager.PushNotificationKeys.MESSAGE;
import static club.idiom.app.utils.PushNotificationManager.PushNotificationKeys.TYPE;

/**
 * Created by marcoscardoso on 09/04/16.
 */
public class ChatMessage implements Parcelable {
    private int id;
    private String message;
    private int self;
    private int old;
    private long sent;
    private int from;
    private int fid;
    private int message_type;
    private String callid;

    // Utility field (does not come from server)
    private ChatTypeEnum typeEnum;
    private ChatStateEnum state;

    public ChatMessage(int id, String message, int self, int from, ChatTypeEnum typeEnum, ChatStateEnum state) {
        this.id = id;
        this.message = message;
        this.self = self;
        this.old = 0;
        this.sent = new Date().getTime();
        this.from = from;
        this.typeEnum = typeEnum;
        this.message_type = typeEnum.getValue();
        this.state = state;
    }

    /**
     * Getters
     **/

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message == null ? "" : message;
    }

    public boolean isFromMyself() {
        return self != 0;
    }

    public boolean isOld() {
        return old != 0;
    }

    public long getSentTimestamp() {
        return sent;
    }

    public Date getSent() {
        return sent == 0 ? null : new Date(sent);
    }

    public int getFrom() {
        return from > 0 ? from : fid;
    }

    public ChatTypeEnum getMessageType() {
        if (typeEnum == null) {
            typeEnum = ChatTypeEnum.getTypeForInt(message_type);
        }
        return typeEnum;
    }

    public ChatStateEnum getState() {
        return state == null ? ChatStateEnum.unknown : state;
    }

    public void setState(ChatStateEnum state) {
        this.state = state;
    }

    public static ChatMessage fromMessageSentJson(JSONObject jsonObject) {
        try {
            int id = jsonObject.getInt("id");
            String message = jsonObject.getString("m");
            int from = jsonObject.getInt("from");

            ChatMessage chatMessage = new ChatMessage(id, message, 1, from, ChatTypeEnum.text, ChatStateEnum.sent);
            return chatMessage;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ChatMessage fromMessageReceivedJson(JSONObject jsonObject) {
        try {
            Gson gson = ApiFactory.getGsonInstance();
            ChatMessage message = gson.fromJson(jsonObject.toString(), ChatMessage.class);
            message.typeEnum = ChatTypeEnum.getTypeForInt(message.message_type);
            message.setState(ChatStateEnum.sentAndReceived);
            return message;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ChatMessage fromPushMessageJson(JSONObject payload) {
        final String AVCHAT_INCOMING_CALL = "O_AVC";
        final String AUDIOCHAT_INCOMING_CALL = "O_AC";

        try {
            JSONObject messageJson = payload.getJSONObject(MESSAGE);

            ChatMessage message = new ChatMessage();
            message.setState(ChatStateEnum.sentAndReceived);

            if (messageJson.has(FROM_ID)) {
                message.from = messageJson.getInt(FROM_ID);
            }

            if (messageJson.has(ID)) {
                message.id = messageJson.getInt(ID);
            }

            if (payload.has(CALL_ID)) {
                message.setCallid(payload.getString(CALL_ID));
            }

            if (payload.has(TYPE)) {
                String type = payload.getString(TYPE);
                if (AVCHAT_INCOMING_CALL.equals(type)) {
                    message.typeEnum = ChatTypeEnum.video;
                } else if (AUDIOCHAT_INCOMING_CALL.equals(type)) {
                    message.typeEnum = ChatTypeEnum.sound;
                } else {
                    message.typeEnum = ChatTypeEnum.unknown;
                }
                message.message_type = message.typeEnum.getValue();
            }

            if (payload.has(ALERT)) {
                message.message = payload.getString(ALERT);
                if (message.message.contains(":")) {
                    String[] parts = message.message.split(":");
                    message.message = parts[1];
                }
            }

            return message;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parcelable Methods
     **/

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.message);
        dest.writeInt(this.self);
        dest.writeInt(this.old);
        dest.writeLong(this.sent);
        dest.writeInt(this.from);
        dest.writeInt(this.fid);
        dest.writeInt(this.message_type);
        dest.writeInt(this.typeEnum == null ? -1 : this.typeEnum.ordinal());
        dest.writeInt(this.state == null ? -1 : this.state.ordinal());
    }

    public ChatMessage() {
    }

    public ChatMessage(String message) {
        this.message = message;
        state = ChatStateEnum.sending;
        sent = (new Date()).getTime();
    }

    protected ChatMessage(Parcel in) {
        this.id = in.readInt();
        this.message = in.readString();
        this.self = in.readInt();
        this.old = in.readInt();
        this.sent = in.readLong();
        this.from = in.readInt();
        this.fid = in.readInt();
        this.message_type = in.readInt();
        int tmpTypeEnum = in.readInt();
        this.typeEnum = tmpTypeEnum == -1 ? null : ChatTypeEnum.values()[tmpTypeEnum];
        int tmpState = in.readInt();
        this.state = tmpState == -1 ? null : ChatStateEnum.values()[tmpState];
    }

    public static final Creator<ChatMessage> CREATOR = new Creator<ChatMessage>() {
        @Override
        public ChatMessage createFromParcel(Parcel source) {
            return new ChatMessage(source);
        }

        @Override
        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    public String getCallid() {
        return callid;
    }

    public void setCallid(String callid) {
        this.callid = callid;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSelf() {
        return self;
    }
}