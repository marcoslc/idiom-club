package club.idiom.app.utils;

import android.util.Log;

import com.inscripts.interfaces.SubscribeCallbacks;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.models.events.CometChatEvent.GotAnnouncementEvent;
import club.idiom.app.models.events.CometChatEvent.GotProfileInfoEvent;
import club.idiom.app.models.events.CometChatEvent.OnAVChatMessageReceivedEvent;
import club.idiom.app.models.events.CometChatEvent.OnActionMessageReceivedEvent;

import static club.idiom.app.models.events.CometChatEvent.GotOnlineListEvent;
import static club.idiom.app.models.events.CometChatEvent.OnErrorEvent;
import static club.idiom.app.models.events.CometChatEvent.OnMessageReceivedEvent;

/**
 * Created by Victor on 19/04/2016.
 */
public class BasicSubscribeCallbacks implements SubscribeCallbacks {
    private static final String LOGTAG = BasicSubscribeCallbacks.class.getSimpleName();

    private boolean printLog = false;

    public BasicSubscribeCallbacks(boolean printLog) {
        this.printLog = printLog;
    }

    @Override
    public void gotOnlineList(JSONObject jsonObject) {
        if (printLog) {
            Log.d(LOGTAG, "gotOnlineList: " + jsonObject);
        }
        EventBus.getDefault().post(new GotOnlineListEvent(jsonObject));
    }

    @Override
    public void onError(JSONObject jsonObject) {
        if (printLog) {
            Log.e(LOGTAG, "onError: " + jsonObject);
        }
        EventBus.getDefault().post(new OnErrorEvent(jsonObject));
    }

    @Override
    public void onMessageReceived(JSONObject jsonObject) {
        if (printLog) {
            Log.d(LOGTAG, "onMessageReceived: " + jsonObject);
        }

        ChatMessage message = ChatMessage.fromMessageReceivedJson(jsonObject);

        if (message.getMessage() != null) {

            if (message.getMessage().contains(ActionsKeys.ACTION))
                EventBus.getDefault().post(new CometChatEvent.OnActionReceivedEvent(jsonObject));

            else
                EventBus.getDefault().post(new OnMessageReceivedEvent(jsonObject));
        }

    }

    @Override
    public void gotProfileInfo(JSONObject jsonObject) {
        if (printLog) {
            Log.d(LOGTAG, "gotProfileInfo: " + jsonObject);
        }

        EventBus.getDefault().post(new GotProfileInfoEvent(jsonObject));
        PushNotificationManager.subscribe(jsonObject);
    }

    @Override
    public void gotAnnouncement(JSONObject jsonObject) {
        if (printLog) {
            Log.d(LOGTAG, "gotAnnouncement: " + jsonObject);
        }
        EventBus.getDefault().post(new GotAnnouncementEvent(jsonObject));
    }

    @Override
    public void onAVChatMessageReceived(JSONObject jsonObject) {
        if (printLog) {
            Log.d(LOGTAG, "onAVChatMessageReceived: " + jsonObject);
        }
        EventBus.getDefault().post(new OnAVChatMessageReceivedEvent(jsonObject));
    }

    @Override
    public void onActionMessageReceived(JSONObject jsonObject) {
        if (printLog) {
            Log.d(LOGTAG, "onActionMessageReceived: " + jsonObject);
        }
        EventBus.getDefault().post(new OnActionMessageReceivedEvent(jsonObject));
    }
}