package club.idiom.app.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import club.idiom.app.R;

public class Utils {
    public static final int TELEFONE_MIN_LENGTH = 10;
    public static final int SENHA_MIN_LENGTH = 6;

    public static void emailIntent(Context context, String endereco, String assunto) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", endereco, null));
        if (assunto != null)
            intent.putExtra(Intent.EXTRA_SUBJECT, assunto);
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.send_using)));
    }

    public static void webIntent(Context context, String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, R.string.browserNotFoundError, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.actionError, Toast.LENGTH_SHORT).show();
        }
    }

    public static void playIntent(Context context, String packageName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
        } catch (ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }

    public static boolean validateEmail(Context ctx, String email) throws InvalidFormDataException {
        if (TextUtils.isEmpty(email) || !Patterns.EMAIL_ADDRESS.matcher(email).matches())
            throw new InvalidFormDataException(ctx.getString(R.string.invalidEmail));

        return true;
    }

    public static boolean validateName(Context ctx, String nome) throws InvalidFormDataException {
        if (!(nome != null && nome.trim().length() >= SENHA_MIN_LENGTH))
            throw new InvalidFormDataException(ctx.getString(R.string.invalidName));

        return true;
    }

    public static boolean validateAddress(Context ctx, String endereco) throws InvalidFormDataException {
        if (!(endereco != null && endereco.trim().length() >= SENHA_MIN_LENGTH))
            throw new InvalidFormDataException(ctx.getString(R.string.invalidAddress));

        return true;
    }

    public static boolean validatePhone(Context ctx, String telefone) throws InvalidFormDataException {
        if (!(telefone != null && telefone.length() >= TELEFONE_MIN_LENGTH))
            throw new InvalidFormDataException(ctx.getString(R.string.invalidTelefone));

        return true;
    }

    public static boolean validatePassword(Context ctx, String senha) throws InvalidFormDataException {
        if (!(senha != null && senha.length() >= SENHA_MIN_LENGTH))
            throw new InvalidFormDataException(ctx.getString(R.string.invalidPassword));

        return true;
    }

    public static boolean validatePasswordConfirmation(Context ctx, String senha) throws InvalidFormDataException {
        try {
            return validatePassword(ctx, senha);
        } catch (InvalidFormDataException e) {
            throw new InvalidFormDataException(ctx.getString(R.string.invalidPasswordConfirm));
        }
    }

    public static boolean validateBothPasswords(Context ctx, String senha, String senhaConfirm) throws InvalidFormDataException {
        if (!validatePassword(ctx, senha) || !validatePasswordConfirmation(ctx, senhaConfirm) || !senha.equals(senhaConfirm))
            throw new InvalidFormDataException(ctx.getString(R.string.passwordsDontMatch));

        return true;
    }

    public static void shareIntent(Context ctx, String title, String url) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, title + ": " + url);
            ctx.startActivity(shareIntent);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(ctx, R.string.actionError, Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean canOpenWhatsapp(Context ctx, String title, String link) {
        Intent whatsappIntent = new Intent(Intent.ACTION_VIEW);
        whatsappIntent.setData(Uri.parse("whatsapp://send?text=" + title + ": " + link));
        return ctx.getPackageManager().resolveActivity(whatsappIntent, PackageManager.MATCH_DEFAULT_ONLY) != null;
    }

    public static void shareWhatsapp(Context ctx, String title, String link) {
        Intent whatsappIntent = new Intent(Intent.ACTION_VIEW);
        whatsappIntent.setData(Uri.parse("whatsapp://send?text=" + title + ": " + link));
        ctx.startActivity(whatsappIntent);
    }

    //------

    public static Date getDateInTimezone(String timezone) {
        TimeZone tz = TimeZone.getTimeZone(timezone);
        Calendar c = Calendar.getInstance(tz);
        return c.getTime();
    }

    public static boolean hasPermission(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

}