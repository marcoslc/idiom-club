package club.idiom.app.utils;

import android.content.Context;

import com.inscripts.cometchat.sdk.CometChat;

import club.idiom.app.R;
import club.idiom.app.api.ApiFactory;

/**
 * Created by Victor on 14/04/2016.
 */
public class CometchatUtils {

    public static CometChat getInstance(Context context) {
        return CometChat.getInstance(context, context.getString(R.string.cometchat_api_key));
    }

    public static String getCometchatUrl() {
        return ApiFactory.BASE_URL_PRE + "cometchat/";
    }
}