package club.idiom.app.utils;

import android.app.ProgressDialog;
import android.net.Uri;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

/**
 * Created by victorraft on 09/03/16.
 */
public class ViewUtils {

    public static void dismissDialog(ProgressDialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static void setDrawee(SimpleDraweeView draweeView, int size, String url) {
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(url == null ? "" : url))
                .setResizeOptions(new ResizeOptions(size, size))
                .setAutoRotateEnabled(true)
                .build();

        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setOldController(draweeView.getController())
                .setImageRequest(request)
                .build();


        draweeView.setController(controller);
    }

    public static void setDrawee(SimpleDraweeView draweeView, String url) {
        Uri uri = Uri.parse(url == null ? "" : url);
        draweeView.setImageURI(uri);
    }
}