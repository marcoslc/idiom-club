package club.idiom.app.utils;

/**
 * Created by raft on 15/07/15.
 */
public class InvalidFormDataException extends Exception {
    public InvalidFormDataException(String error) {
        super(error);
    }
}
