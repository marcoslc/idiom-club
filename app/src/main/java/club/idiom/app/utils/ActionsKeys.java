package club.idiom.app.utils;

/**
 * Created by marcoscardoso on 02/08/16.
 */
public class ActionsKeys {
    public static final String ACTION = "[ACTION]";

    public static final String CHANGE_TO = ACTION + "CHANGE_TO";
    public static final String CHANGE_TO_ACCEPTED = ACTION + "CHANGE_TO_ACC";
    public static final String CHANGE_TO_REJECTED = ACTION + "CHANGE_TO_REJ";

}

