package club.idiom.app.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.inscripts.helpers.PreferenceHelper;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.SaveCallback;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Random;

import club.idiom.app.R;
import club.idiom.app.activities.BeforeCallActivity_;
import club.idiom.app.activities.ChatMessageActivity_;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ApiService;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.models.User;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.chatmodel.ChatTypeEnum;
import club.idiom.app.singletons.MySharedPrefs_;
import club.idiom.app.singletons.UserConfig;
import retrofit2.Call;

@EReceiver
public class PushNotificationManager extends ParsePushBroadcastReceiver {
    private static final String LOGTAG = PushNotificationManager.class.getSimpleName();
    private static final String AVCHAT_INCOMING_CALL = "O_AVC";
    private static final String AUDIOCHAT_INCOMING_CALL = "O_AC";

    public static final String CHAT_TAG = "Chat_";
    public static final String CHAT_DESCRIPTION_TAG = "Chat_Description_";

    @SystemService
    NotificationManager notificationManager;

    @Pref
    MySharedPrefs_ prefs;

    @Bean
    UserConfig userConfig;

    public static class PushNotificationKeys {
        public static final String MESSAGE = "m";
        public static final String DATA = "com.parse.Data";
        public static final String AV_CHAT = "avchat";
        public static final String FROM_ID = "fid";
        public static final String ID = "id";
        public static final String IS_CHATROOM = "isCR";
        public static final String ALERT = "alert";
        public static final String IS_ANNOUNCEMENT = "isANN";
        public static final String TYPE = "t";
        public static final String CALL_ID = "grp";
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.v(LOGTAG, "onReceive" + intent.getExtras());

        Bundle bundle = intent.getExtras();

        try {
            if (null != bundle) {
                JSONObject payload = new JSONObject(bundle.getString(PushNotificationKeys.DATA));
                final ChatMessage message = ChatMessage.fromPushMessageJson(payload);

                // If message is from a chat that's already opened then do nothing.
                if (message == null
                        || message.getFrom() == prefs.openChat().get()
                        || message.getFrom() == userConfig.getUser().id
                        || (message.getMessage() != null && message.getMessage().contains(ActionsKeys.ACTION))) {
                    return;
                }

                ApiService apiService = ApiFactory.getApiService();
                apiService.getUserFromId(message.getFrom(), userConfig.getUser().token).enqueue(new IdiomApiCalback() {
                    @Override
                    public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                        if (apiResponse.user != null) {
                            if (message.getMessageType() == ChatTypeEnum.sound || message.getMessageType() == ChatTypeEnum.video) {
                                onReceiveCall(apiResponse.user, context, message);
                            } else {
                                buildNotification(apiResponse.user, message, context);
                            }
                        }
                    }

                    @Override
                    public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                        String message = errorEnum.getErrorDescription(context);
                        Log.e(LOGTAG, message);
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void subscribe(JSONObject jsonObject) {
        if (jsonObject != null && jsonObject.has("push_channel")) {
            try {
                String pushChannel = jsonObject.getString("push_channel");
                FirebaseMessaging.getInstance().subscribeToTopic(pushChannel);
                Log.d(LOGTAG, "subscribeToTopic: " + pushChannel);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void unsubscribe() {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.remove("channels");
        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void saveNotificationIdForThisChat(int notificationId, ChatMessage message) {
        prefs.getSharedPreferences().edit().putInt(CHAT_TAG + message.getFrom(), notificationId).commit();
    }

    private int getNotificationIdForThisChat(ChatMessage message) {
        return prefs.getSharedPreferences().getInt(CHAT_TAG + message.getFrom(), 0);
    }

    private void buildNotification(final User user, ChatMessage message, Context context) {

        if (prefs.openChat().get() == user.id)
            return;

        int notificationNumber = getNotificationIdForThisChat(message);
        if (notificationNumber == 0) {
            Random r = new Random((new Date()).getTime());
            notificationNumber = r.nextInt(500) + 1001;
            saveNotificationIdForThisChat(notificationNumber, message);
        }

        Intent resultIntent = ChatMessageActivity_.intent(context).user(user).notificationId(notificationNumber).get();
        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                context,
                notificationNumber,
                resultIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
        );

        String description = message.getMessage().length() > 160 ? message.getMessage().substring(0, 160) : message.getMessage();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(user.name)
                .setContentText(description)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(getBigMessage(message)))
                .setColor(ContextCompat.getColor(context, R.color.blue_idiomclub))
                //         .setLargeIcon(bigIcon)
                .setSmallIcon(getSmallIconNotification())
                //  .setVibrate(getVibrateSettings())
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true);

        int finalNotificationNumber = notificationNumber;

        Notification notification = builder.build();
        notification.defaults |= Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        notification.ledARGB = 0xff00ff00;
        notification.ledOnMS = 300;
        notification.ledOffMS = 2000;

        if (VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification.priority = Notification.PRIORITY_MAX;
        }
        notificationManager.notify(finalNotificationNumber, notification);
    }

    public static void unsubscribe(String channel) {
        try {
            if (!TextUtils.isEmpty(channel)) {
                ParsePush.unsubscribeInBackground(channel);
            }
            ParseInstallation.getCurrentInstallation().saveInBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);
        onReceive(context, intent);
    }

    /**
     * Clears all the notifications
     */
    public static void clearAllNotifications() {
        NotificationManager notificationManager = (NotificationManager) PreferenceHelper.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        Log.v(LOGTAG, "onPushReceive " + intent.getExtras());
        // onReceive(context, intent);
    }

    private String getBigMessage(ChatMessage message) {
        String big = prefs.getSharedPreferences().getString(CHAT_DESCRIPTION_TAG + message.getFrom(), "");
        big += message.getMessage() + "\n";
        prefs.getSharedPreferences().edit().putString(CHAT_DESCRIPTION_TAG + message.getFrom(), big).commit();
        return big;
    }

    public static int getSmallIconNotification() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

    public static void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    private void onReceiveCall(User user, final Context context, final ChatMessage message) {
        BeforeCallActivity_.intent(context)
                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .isReceiving(true)
                .user(user)
                .isVideo(message.getMessageType() == ChatTypeEnum.video)
                .callId(message.getCallid()).start();
    }
}