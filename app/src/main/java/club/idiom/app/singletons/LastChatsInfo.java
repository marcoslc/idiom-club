package club.idiom.app.singletons;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import club.idiom.app.api.ApiFactory;
import club.idiom.app.models.User;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.events.AppStateEvent.OnAppBackgroundedEvent;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.models.events.CometChatEvent.OnMessageReceivedEvent;
import club.idiom.app.models.events.GotFriendListEvent;

import static org.androidannotations.annotations.EBean.Scope.Singleton;

/**
 * Created by Victor on 28/04/2016.
 */
@EBean(scope = Singleton)
public class LastChatsInfo {
    public static final String LOGTAG = LastChatsInfo.class.getSimpleName();

    @RootContext
    Context context;

    SparseArray<User> userList = new SparseArray<>();
    SparseArray<ChatInfo> chatsList = new SparseArray<>();

    @Pref
    MySharedPrefs_ preferences;

    public static class ChatInfo {
        public User user;
        public ChatMessage lastMessage;
    }

    // This is called right after dependency injection, when preferences and context are ready to use.
    @AfterInject
    void afterInject() {
        initFromPreferences();
        EventBus.getDefault().register(this);
    }

    public ArrayList<ChatInfo> getChatsList() {
        ArrayList<ChatInfo> result = new ArrayList<>();

        for (int i = 0; i < chatsList.size(); i++) {
            result.add(chatsList.valueAt(i));
        }

        return result;
    }

    public void initFromPreferences() {
        String jsonLastMessages = preferences.lastMessages().get();
        Gson gson = ApiFactory.getGsonInstance();
        try {
            List<ChatInfo> list = gson.fromJson(jsonLastMessages, new TypeToken<ArrayList<ChatInfo>>() {
            }.getType());

            if (list != null && !list.isEmpty()) {
                for (ChatInfo chatInfo : list) {
                    chatsList.put(chatInfo.user.id, chatInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void persist() {
        Gson gson = ApiFactory.getGsonInstance();
        String jsonLastMessages = gson.toJson(getChatsList());

        preferences.edit()
                .lastMessages().put(jsonLastMessages)
                .apply();
    }

    public void clean() {
        preferences.edit()
                .lastMessages().remove()
                .apply();
    }

    private void addMessage(ChatMessage message) {
        if (message != null) {
            User user = userList.get(message.getFrom());
            if (user == null) {
                Log.e(LOGTAG, "No user info available to save message.");
            } else {
                ChatInfo chatInfo = new ChatInfo();
                chatInfo.user = user;
                chatInfo.lastMessage = message;
                chatsList.put(chatInfo.user.id, chatInfo);
            }
        }
    }

    @Subscribe
    public void onMessageReceived(OnMessageReceivedEvent messageReceived) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(messageReceived.jsonObject);
        addMessage(message);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnMessageSent(CometChatEvent.OnMessageSentEvent event) {
        ChatMessage message = ChatMessage.fromMessageSentJson(event.jsonObject);
        addMessage(message);
    }

    @Subscribe
    public void gotFriendListEvent(GotFriendListEvent event) {
        List<User> list = event.users;
        if (list != null && !list.isEmpty()) {
            for (User u : list) {
                userList.put(u.id, u);
            }
        }
    }

    @Subscribe
    public void onAppBackgroundedEvent(OnAppBackgroundedEvent event) {
        persist();
    }
}