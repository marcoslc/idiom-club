package club.idiom.app.singletons;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.List;

import club.idiom.app.models.chatmodel.ChatMessage;

/**
 * Created by marcoscardoso on 28/04/16.
 */

@EBean(scope = EBean.Scope.Singleton)
public class ChatPersist {
    private static final String KEY_CHAT_HISTORY = "CHAT_HISTORY_";

    @RootContext
    Context context;

    @Pref
    MySharedPrefs_ preferences;

    @AfterInject
    void afterInject() {

    }

    public void saveChatHistoryById(int userId, List<ChatMessage> list) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(list);

        SharedPreferences.Editor prefsEditor = preferences.getSharedPreferences().edit();
        prefsEditor.putString(KEY_CHAT_HISTORY + userId, jsonString);
        prefsEditor.commit();
    }

    public List<ChatMessage> getChatHistoryById(int userId) {
        SharedPreferences prefsEditor = preferences.getSharedPreferences();
        String messagesString = prefsEditor.getString(KEY_CHAT_HISTORY + userId, "");

        Gson gson = new Gson();
        ArrayList<ChatMessage> list = gson.fromJson(messagesString, new TypeToken<ArrayList<ChatMessage>>() {
        }.getType());

        return list;
    }
}
