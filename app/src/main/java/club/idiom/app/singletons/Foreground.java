package club.idiom.app.singletons;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.api.BackgroundExecutor;
import org.greenrobot.eventbus.EventBus;

import club.idiom.app.application.MyApplication;
import club.idiom.app.models.events.AppStateEvent.OnAppBackgroundedEvent;
import club.idiom.app.models.events.AppStateEvent.OnAppForegroundedEvent;

import static org.androidannotations.annotations.EBean.Scope.Singleton;

/**
 * Created by Victor on 03/05/2016.
 */
@EBean(scope = Singleton)
public class Foreground implements Application.ActivityLifecycleCallbacks {
    private static final String LOGTAG = Foreground.class.getSimpleName();

    private static final long CHECK_BG_DELAY_MS = 500;

    @App
    MyApplication application;

    private boolean foreground = false, paused = true;

    @AfterInject
    public void init() {
        application.registerActivityLifecycleCallbacks(this);
    }

    /**
     * Activity Lifecycle Callbacks
     **/

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        paused = false;
        boolean wasBackground = !foreground;
        foreground = true;

        BackgroundExecutor.cancelAll("check_bg_task", true);

        if (wasBackground) {
            Log.d(LOGTAG, "went foreground");
            EventBus.getDefault().post(new OnAppForegroundedEvent());
        } else {
            Log.d(LOGTAG, "still foreground");
        }
    }

    @Background(id = "check_bg_task", delay = CHECK_BG_DELAY_MS)
    public void checkIfIsBackground() {
        if (foreground && paused) {
            foreground = false;
            Log.d(LOGTAG, "went background");
            EventBus.getDefault().post(new OnAppBackgroundedEvent());
        } else {
            Log.d(LOGTAG, "still foreground");
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        paused = true;

        BackgroundExecutor.cancelAll("check_bg_task", true);
        checkIfIsBackground();
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}