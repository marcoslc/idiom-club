package club.idiom.app.singletons;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Created by Victor on 02/03/2016.
 */
@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface MySharedPrefs {

    String userJson();
    String notificationStack();
    String lastMessages();
    int openChat();
}