package club.idiom.app.singletons;

import android.content.Context;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import club.idiom.app.api.ApiFactory;
import club.idiom.app.models.User;

/**
 * Created by marcoscardoso on 02/03/16.
 */
@EBean(scope = Scope.Singleton)
public class UserConfig {

    @RootContext
    Context context;

    @Pref
    MySharedPrefs_ preferences;

    private User currentUser;

    // This is called right after dependency injection, when preferences and context are ready to use.
    @AfterInject
    void afterInject() {
        currentUser = getUserFromSharedPreferences();
    }

    private User getUserFromSharedPreferences() {
        String jsonUser = preferences.userJson().get();
        Gson gson = ApiFactory.getGsonInstance();
        User user = gson.fromJson(jsonUser, User.class);

        return user;
    }

    public User getUser() {
        if (currentUser == null)
            currentUser = getUserFromSharedPreferences();

        return currentUser;
    }

    public boolean isLoggedIn() {
        return currentUser != null;
    }

    public void saveUser(User user) {
        if (user != null) {
            Gson gson = ApiFactory.getGsonInstance();
            String json = gson.toJson(user);
            preferences.userJson().put(json);
            currentUser = user;
        }
    }

    public void logout() {
        preferences.edit()
                .userJson()
                .remove()
                .apply();

        // Logout from Facebook

        try {
            LoginManager.getInstance().logOut();
        } catch (Exception e) {

        }

        currentUser = null;
    }

    public interface LoginCallback {
        void result(User user, Exception e);
    }
}