package club.idiom.app.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Victor on 02/03/2016.
 */
public class ApiFactory {
    public static final String BASE_URL_PRE = "http://api.idiom.club/";
    private static final String BASE_URL = BASE_URL_PRE + "v1/";

    public static ApiService getApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(getGsonInstance()))
                .build();

        ApiService apiService = retrofit.create(ApiService.class);
        return apiService;
    }

    public static Gson getGsonInstance() {
        return new GsonBuilder()
                .create();
    }
}