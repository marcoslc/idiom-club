package club.idiom.app.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Victor on 02/03/2016.
 */
public interface ApiService {

    @FormUrlEncoded
    @POST("facebook/auth")
    Call<ApiResponse> authFacebook(@Field("token") String fbToken);

    @FormUrlEncoded
    @POST("user")
    Call<ApiResponse> saveUser(@Field("user") String userJson);

    @FormUrlEncoded
    @PATCH("user")
    Call<ApiResponse> updateUser(@Field("token") String token, @Field("user") String userJson);

    @GET("user/{userId}")
    Call<ApiResponse> getUserFromId(@Path("userId") int userId, @Query("token") String token);

    @FormUrlEncoded
    @POST("auth")
    Call<ApiResponse> doLogin(@Field("email") String email, @Field("password") String password, @Field("token") String token);

    @FormUrlEncoded
    @POST("logout")
    Call<ApiResponse> doLogout(@Field("token") String token);

    @GET("users")
    Call<ApiResponse> listUsers(@Query("token") String token);

    @FormUrlEncoded
    @POST("call/tick")
    Call<ApiResponse> callTick(@Field("token") String token, @Field("user") int userId, @Field("isteacher") int isTeacher);

    // Friend Requests
    @GET("following")
    Call<ApiResponse> listFollowing(@Query("token") String token);

    @GET("followers")
    Call<ApiResponse> listFollowers(@Query("token") String token);

    @FormUrlEncoded
    @POST("follow/{userId}")
    Call<ApiResponse> followUser(@Field("token") String token, @Path("userId") String userId);

    @FormUrlEncoded
    @POST("unfollow/{userId}")
    Call<ApiResponse> unfollowUser(@Field("token") String token, @Path("userId") String userId);

    @FormUrlEncoded
    @POST("user/forgot")
    Call<ApiResponse> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("user/recover")
    Call<ApiResponse> recoverPassword(@Field("token") String token, @Field("password") String password);
}