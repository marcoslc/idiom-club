package club.idiom.app.api;

import java.util.List;

import club.idiom.app.models.User;

/**
 * Created by Victor on 02/03/2016.
 */
public class ApiResponse {
    public int error;
    public String error_msg;

    public User user;
    public List<User> users, following, followers;

    public float tml;
}