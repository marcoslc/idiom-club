package club.idiom.app.api;

import android.content.Context;

import club.idiom.app.R;

/**
 * Created by victorraft on 07/03/16.
 */
public enum ErrorEnum {
    unknown(-1),
    ok(0),
    wrong_email(1),
    wrong_password(2),
    invalid_token(3),
    invalid_facebook_token(4),
    email_send_error(5),
    invalid_fp_token(7),    //fp = Forguet Password
    expired_fp_token(8);

    private final int value;

    ErrorEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ErrorEnum getErrorForInt(int errorCode) {
        for (ErrorEnum error : ErrorEnum.values()) {
            if (error.getValue() == errorCode) {
                return error;
            }
        }

        return unknown;
    }

    public String getErrorDescription(Context ctx) {
        switch (this) {
            case wrong_email:
                return ctx.getString(R.string.error_wrong_email);
            case wrong_password:
                return ctx.getString(R.string.error_wrong_password);
            case invalid_token:
                return ctx.getString(R.string.error_invalid_token);
            case email_send_error:
                return ctx.getString(R.string.error_request_recover_password);
            case invalid_fp_token:
                return ctx.getString(R.string.error_forget_password_token);
            case expired_fp_token:
                return ctx.getString(R.string.error_forget_password_expired_token);
            default:
                return ctx.getString(R.string.error_unknown);
        }
    }
}