package club.idiom.app.api;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Victor on 28/12/2015.
 */
public abstract class IdiomApiCalback implements Callback<ApiResponse> {
    public static final String LOGTAG = IdiomApiCalback.class.getSimpleName();

    public class IdiomThrowable extends Throwable {
        ErrorEnum errorEnum;

        public IdiomThrowable(String message, ErrorEnum errorEnum) {
            super(message);
            this.errorEnum = errorEnum;
        }
    }

    @Override
    public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
        if (response == null) {
            onFailure(call, new Throwable("Response is null"));
            return;
        }

        ApiResponse apiResponse = response.body();
        if (apiResponse == null) {
            onFailure(call, new Throwable("ApiResponse is null"));
            return;
        }

        if (apiResponse.error != 0) {
            ErrorEnum errorEnum = ErrorEnum.getErrorForInt(apiResponse.error);
            onFailure(call, new IdiomThrowable(apiResponse.error_msg, errorEnum));
            return;
        }

        onSuccess(call, apiResponse);
    }

    @Override
    public void onFailure(Call<ApiResponse> call, Throwable t) {
        Log.e(LOGTAG, "ERROR: " + (t == null ? "null" : t.getMessage()));

        if (t instanceof IdiomThrowable) {
            IdiomThrowable idiomThrowable = (IdiomThrowable) t;

            if (idiomThrowable.errorEnum == ErrorEnum.invalid_token) {
                // Invalid token. Logout.
                // todo
            } else {
                onFail(call, idiomThrowable.errorEnum);
            }
        } else {
            onFail(call, ErrorEnum.unknown);
        }

    }

    // ----- Methods that should be overwritten

    public abstract void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse);

    public abstract void onFail(Call<ApiResponse> call, ErrorEnum errorEnum);
}