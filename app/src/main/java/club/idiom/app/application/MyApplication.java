package club.idiom.app.application;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;
import com.orm.SugarContext;
import com.parse.Parse;
import com.parse.ParseInstallation;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;
import org.json.JSONObject;

import club.idiom.app.activities.LoginActivity_;
import club.idiom.app.activities.RootActivity_;
import club.idiom.app.models.User;
import club.idiom.app.notification.MyFirebaseInstanceIdService;
import club.idiom.app.singletons.Foreground;
import club.idiom.app.singletons.LastChatsInfo;
import club.idiom.app.singletons.UserConfig;
import club.idiom.app.utils.BasicSubscribeCallbacks;
import club.idiom.app.utils.CometchatUtils;
import club.idiom.app.utils.PushNotificationManager;
import io.fabric.sdk.android.Fabric;

import static club.idiom.app.BuildConfig.IS_DEBUG;

/**
 * Created by victorraft on 23/02/16.
 */
@EApplication
public class MyApplication extends MultiDexApplication {
    public static final String LOGTAG = MyApplication.class.getSimpleName();
    private static final long CHECK_BG_DELAY_MS = 500;

    @Bean
    UserConfig userConfig;
    @Bean
    LastChatsInfo lastChatsInfo;
    @Bean
    Foreground foreground;

    CometChat cometChat;

    @Override
    public void onCreate() {
        super.onCreate();

        cometChat = CometchatUtils.getInstance(getApplicationContext());

        try {
            Parse.enableLocalDatastore(this);
            Parse.initialize(this, "JTsXPoBuAIgZnxkIQVcfXIY6ntiCXzTIa44L1b9i", "HMJ8W3d3gn77j1W3XQlwyyjPqnIuqWQyKPVhXajk");
        } catch (Exception e) {
            e.printStackTrace();
        }

        SugarContext.init(this);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        FacebookSdk.sdkInitialize(getApplicationContext());

        // On Android 4.4 KitKat Fresco's downsampling uses more memory than resizing, so don't enable it.
        // See: http://frescolib.org/docs/resizing-rotating.html#downsampling
        boolean shouldEnableDownsample = Build.VERSION.SDK_INT != Build.VERSION_CODES.KITKAT;

        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(shouldEnableDownsample)
                .build();

        Fresco.initialize(this, config);


        if (userConfig.getUser() != null)
            cometChat.subscribe(false, new BasicSubscribeCallbacks(true));

        if (!IS_DEBUG) {
            Fabric.with(this, new Crashlytics());
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        SugarContext.terminate();
        cometChat.unsubscribe();
        super.onTerminate();
    }

    public void makeLogin(Activity activity, User user) {
        if (user == null) {
            return;
        }

        userConfig.saveUser(user);
        Toast.makeText(this, "Login successful!", Toast.LENGTH_SHORT).show();
        RootActivity_.intent(activity).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
        cometChat.subscribe(false, new BasicSubscribeCallbacks(true));
    }

    public void makeLogout(Activity activity) {
        final CometChat cometChat = CometchatUtils.getInstance(this);
        cometChat.unsubscribe();
        cometChat.logout(new Callbacks() {
            @Override
            public void successCallback(JSONObject response) {
                Log.d(LOGTAG, "cometchat logout success");
            }

            @Override
            public void failCallback(JSONObject response) {
                Log.e(LOGTAG, "cometchat logout failure" + response);
            }
        });

        PushNotificationManager.unsubscribe();
        lastChatsInfo.clean();
        userConfig.logout();
        activity.finish();
        LoginActivity_.intent(activity).start();
    }
}