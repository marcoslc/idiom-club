package club.idiom.app.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.inscripts.cometchat.sdk.AVChat;
import com.inscripts.cometchat.sdk.AudioChat;
import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.api.BackgroundExecutor;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import club.idiom.app.R;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.models.User;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.utils.ActionsKeys;
import club.idiom.app.utils.CometchatUtils;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

/**
 * Created by marcoscardoso on 08/04/16.
 */

@EActivity(R.layout.activity_chat_video_audio)
public class CallAudioVideoActivity extends BaseActivity {
    private static final String LOGTAG = CallAudioVideoActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_CAMERA_AUDIO = 357;
    private static final long TIME_OUT = 20000;

    @Extra
    User user;
    @Extra
    String callId = "";
    @Extra
    boolean isVideo;
    @Extra
    boolean isReceiving;

    @ViewById
    RelativeLayout callContainer;
    @ViewById
    SimpleDraweeView avatar;

    @ViewById
    TextView tmlTv;

    CometChat cometChat;
    AVChat avChat;
    AudioChat audioChat;

    @ViewById
    FloatingActionButton toggleMicFab, toggleVideoFab, toggleSwitchCameraFab;

    @ViewById
    FloatingActionsMenu rootFab;

    @ViewById
    Switch switchTo;

    boolean videoEnabled = true, audioEnabled = true;
    private PowerManager.WakeLock wakeLock;
    private boolean changeToAwnsered = false;

    ProgressDialog changeToDialog;
    AlertDialog requestChangeTodialog;
    AlertDialog messageReceivedDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    void init() {
        cometChat = CometchatUtils.getInstance(this);

        if (isVideo) {
            avChat = AVChat.getAVChatInstance(this);
            avChat.toggleVideo(videoEnabled);
            avChat.toggleAudio(audioEnabled);
            checkPermissionsAndStartCall();
        } else {
            toggleVideoFab.setVisibility(View.GONE);
            toggleSwitchCameraFab.setVisibility(View.GONE);
            audioChat = AudioChat.getInstance(this);
            audioChat.toggleAudio(audioEnabled);
            checkPermissionsAndStartCall();
            callContainer.setVisibility(View.GONE);
        }

        switchTo.setChecked(isReceiving);
        setUserInfo();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        scheduleNextTick();
        tmlTv.setText(String.valueOf(userConfig.getUser().tml));
        prefs.edit().openChat().put(user.id).apply();
    }

    @Override
    protected void onPause() {
        if (isVideo) {
            avChat.removeVideoOnRotation(callContainer);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        endCall();
        BackgroundExecutor.cancelAll("tick", true);
        prefs.edit().openChat().put(0).apply();
        super.onStop();
    }

    private void sendTick() {
        Log.v(LOGTAG, "sendTick -> " + (isReceiving ? 1 : 0));

        isReceiving = !switchTo.isChecked();
        final Call<ApiResponse> loginCall = apiService.callTick(userConfig.getUser().token, user.id, isReceiving ? 1 : 0);
        loginCall.enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                Log.d(CallAudioVideoActivity.LOGTAG, "callTick success");

                // Update user's TML
                User u = userConfig.getUser();
                u.tml = apiResponse.tml;

                Log.v(CallAudioVideoActivity.LOGTAG, "sendTick response -> " + u.tml);

                userConfig.saveUser(u);

                tmlTv.setText("" + u.tml);

                if (apiResponse.tml <= 0) {
                    showToast(R.string.not_enough_tml_to_continue);
                    endCall();
                } else {
                    scheduleNextTick();
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                Log.e(LOGTAG, "callTick error");
                showToast(R.string.error_getting_tml);
                endCall();
            }
        });
    }

    @Background(delay = 60000, id = "tick")
    public void scheduleNextTick() {
        sendTick();
    }

    private void checkPermissionsAndStartCall() {
        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])) {
                // Show an expanation to the user. After the user sees the explanation, try again to request the permission.
                showRationale(permissions);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CAMERA_AUDIO);
            }
        } else {
            onPermissionsGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CAMERA_AUDIO) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults.length == permissions.length
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted! Do the task you need to do.
                onPermissionsGranted();
            } else {
                // Permission denied! Disable the functionality that depends on this permission.
                onPermissionsDenied();
            }
        }
    }

    public void showRationale(final String[] permissions) {
        new AlertDialog.Builder(this).setMessage(R.string.av_permission_rationale)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(CallAudioVideoActivity.this, permissions, PERMISSION_REQUEST_CAMERA_AUDIO);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onPermissionsDenied();
                    }
                })
                .show();
    }

    void onPermissionsGranted() {
        startCallUi();
    }

    void onPermissionsDenied() {
        finish();
    }

    private void setUserInfo() {
        apiService.getUserFromId(user.id, userConfig.getUser().token).enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                if (apiResponse.user != null) {
                    ViewUtils.setDrawee(avatar, apiResponse.user.getAvatarUrl());
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                String message = errorEnum.getErrorDescription(CallAudioVideoActivity.this);
                showToast(message);
            }
        });
    }

    @Subscribe
    public void OnAVChatMessageReceived(CometChatEvent.OnAVChatMessageReceivedEvent avChatMessageReceived) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(avChatMessageReceived.jsonObject);

        if (message != null) {
            switch (message.getMessageType()) {
                case accepted_call:
                    break;

                case finish_call:
                    endCall();
                    break;
            }
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void startCallUi() {
        Log.d(LOGTAG, "starting call: " + callId);

        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Lock");
        wakeLock.acquire();

        if (isVideo) {
            avChat.startAVChatCall(callId, callContainer, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "startAVChatCall success: " + jsonObject);
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "startAVChatCall fail: " + jsonObject);
                    showToast(R.string.call_error);
                    finish();
                }
            });

            try {
                avChat.addVideoOnRotation(callContainer);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            audioChat.startAudioChatCall(callId, callContainer, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "startAudioChatCall success: " + jsonObject);
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "startAudioChatCall fail: " + jsonObject);
                    showToast(R.string.call_error);
                    finish();
                }
            });
        }
    }

    public void endCall() {

        if (isVideo) {
            avChat.endAVChatCall(String.valueOf(user.id), callId, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "endAVChatCall success: " + jsonObject);
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "endAVChatCall fail: " + jsonObject);
                }
            });
        } else {
            audioChat.endAudioChatCall(String.valueOf(user.id), callId, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "endAVChatCall success: " + jsonObject);
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "endAVChatCall fail: " + jsonObject);
                }
            });
        }

        try {
            avChat.removeVideoOnRotation(callContainer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void buildAndShowSendMessageDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this).setTitle(R.string.send_message);

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        Resources r = getResources();
        int pxMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics());

        lp.setMargins(pxMargin, pxMargin, pxMargin, pxMargin);
        input.setLayoutParams(lp);
        input.setPadding(30, 30, 30, 30);
        dialog.setView(input);

        dialog.setPositiveButton(R.string.send, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String text = input.getText().toString();

                cometChat.sendMessage(String.valueOf(user.id), text, new Callbacks() {
                    @Override
                    public void successCallback(JSONObject jsonObject) {

                    }

                    @Override
                    public void failCallback(JSONObject jsonObject) {
                        Toast.makeText(CallAudioVideoActivity.this, "Message not sent", Toast.LENGTH_LONG).show();
                    }
                });


            }
        });

        dialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void buildAndShowMessageReceivedDialog(String message) {
        if (messageReceivedDialog != null && messageReceivedDialog.isShowing()) {
            messageReceivedDialog.setMessage(message);
            return;
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(this).setTitle(R.string.message_received).setMessage(message);

        dialog.setPositiveButton(R.string.awnser, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buildAndShowSendMessageDialog();
            }
        });

        dialog.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        messageReceivedDialog = dialog.show();
    }

    @Click
    void finishBtClicked() {
        endCall();
    }


    @Click
    void toggleMicFabClicked() {
        audioEnabled = !audioEnabled;

        if (isVideo) {
            avChat.toggleAudio(!audioEnabled);
        } else {
            audioChat.toggleAudio(!audioEnabled);
        }

        toggleMicFab.setIcon(audioEnabled ? R.drawable.ic_av_mic : R.drawable.ic_av_mic_off);
    }

    @Click
    void toggleVideoFabClicked() {
        videoEnabled = !videoEnabled;
        avChat.toggleVideo(videoEnabled);
        toggleVideoFab.setIcon(videoEnabled ? R.drawable.ic_videocam : R.drawable.ic_av_videocam_off);

    }

    @Click
    void sendTextFabClicked() {
        buildAndShowSendMessageDialog();

        rootFab.collapse();
    }

    @Click
    void toggleSwitchCameraFabClicked() {
        avChat.switchCamera();
    }

    @Click
    void switchToClicked() {
        buildAndShowDialogToConfirmChange();
    }

    private void buildAndShowDialogToConfirmChange() {

        new AlertDialog.Builder(this)
                .setMessage(R.string.confirmation_change_to)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startChangeToProcess();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void startChangeToProcess() {
        changeToDialog = new ProgressDialog(this);
        changeToDialog.setCancelable(false);
        changeToDialog.setMessage(getString(R.string.waiting_awnser_other_user));

        cometChat.sendMessage(String.valueOf(user.id), ActionsKeys.CHANGE_TO, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                changeToDialog.show();
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!changeToAwnsered) {
                            changeToDialog.dismiss();
                            Toast.makeText(CallAudioVideoActivity.this, R.string.rejected, Toast.LENGTH_LONG).show();
                            if (switchTo != null) {
                                switchTo.setChecked(!switchTo.isChecked());
                            }
                        }
                    }
                }, TIME_OUT);
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isVideo) {
            avChat.endAVChatCall(String.valueOf(user.id), callId, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {

                }

                @Override
                public void failCallback(JSONObject jsonObject) {

                }
            });
        }

    }

    @Subscribe
    public void onAVChatMessageReceived(CometChatEvent.OnAVChatMessageReceivedEvent event) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(event.jsonObject);

        if (message != null) {
            switch (message.getMessageType()) {
                case accepted_call:
                    break;
                case receiving_call:
                    break;
                case finish_call:
                    endCall();
                    break;
                case rejected_call:
                    break;
                case cancelled_call:
                    break;
                case timeout_call:
                    break;
                case destinatary_busy_call:
                    break;
                case unknown:
                    break;
            }
        }
    }

    @Subscribe
    public void onMessageReceived(CometChatEvent.OnActionReceivedEvent messageReceived) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(messageReceived.jsonObject);


        if (message != null && message.getSelf() != 1) {

            if (requestChangeTodialog != null && requestChangeTodialog.isShowing())
                return;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setCancelable(false);

            if (message.getMessage().equals(ActionsKeys.CHANGE_TO)) {
                builder.setMessage(R.string.change_to_requested);
                builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        sendMessageChangeToAwnser(false);
                    }
                });
                builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendMessageChangeToAwnser(true);
                        isReceiving = !isReceiving;
                    }
                });

                requestChangeTodialog = builder.show();
            }

            if (changeToDialog != null && changeToDialog.isShowing()) {
                if (message.getMessage().equals(ActionsKeys.CHANGE_TO_ACCEPTED)) {
                    changeToAwnsered = true;
                    if (changeToDialog != null) {
                        changeToDialog.dismiss();
                        isReceiving = !isReceiving;
                        Toast.makeText(CallAudioVideoActivity.this, R.string.accepted, Toast.LENGTH_LONG).show();
                    }
                }
                if (message.getMessage().equals(ActionsKeys.CHANGE_TO_REJECTED)) {
                    changeToAwnsered = true;
                    if (changeToDialog != null) {
                        changeToDialog.dismiss();

                        if (switchTo != null) {
                            switchTo.setChecked(!switchTo.isChecked());
                            Toast.makeText(CallAudioVideoActivity.this, R.string.rejected, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        }
    }


    @Subscribe
    public void onMessageReceived(CometChatEvent.OnMessageReceivedEvent messageReceived) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(messageReceived.jsonObject);

        if (message != null && message.getSelf() != 1) {

            buildAndShowMessageReceivedDialog(message.getMessage());
        }
    }


    private void sendMessageChangeToAwnser(final boolean accepted) {
        cometChat.sendMessage(String.valueOf(user.id), accepted ? ActionsKeys.CHANGE_TO_ACCEPTED : ActionsKeys.CHANGE_TO_REJECTED, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                if (accepted) {
                    switchTo.setChecked(!switchTo.isChecked());
                    isReceiving = !isReceiving;
                }
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });
    }
}