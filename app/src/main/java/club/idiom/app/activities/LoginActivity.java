package club.idiom.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.VideoView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;

import club.idiom.app.R;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.fragments.SelectIdiomFragment;
import club.idiom.app.fragments.SelectIdiomFragment_;
import club.idiom.app.fragments.SignupFragment;
import club.idiom.app.fragments.SignupFragment_;
import club.idiom.app.fragments.StartFragment_;
import club.idiom.app.models.User;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity {
    public static String LOGTAG = LoginActivity.class.getSimpleName();

    @ViewById
    VideoView videoView;

    @Extra
    boolean editProfile = false;

    private CallbackManager callbackManager;
    private ProgressDialog fbLoginDialog;

    private View logoToShare;
    private String tagForShare;

    @AfterViews
    public void init() {
        getSupportActionBar().hide();

        User user = userConfig.getUser();

        if (user != null) {
            if (!user.needRegisterIdiom()) {
                app.makeLogin(this, user);
            } else {
                openSelectIdiomFragment(user);
            }
        } else {
            switchToFragment(StartFragment_.builder().build(),false);
        }

        startVideoInBackground();
        setToolbarAndStatusBar();
        setupFacebookLogin();

        fbLoginDialog = new ProgressDialog(this);
        fbLoginDialog.setIndeterminate(true);
        fbLoginDialog.setCancelable(false);
        fbLoginDialog.setMessage(getString(R.string.doing_login_fb));
    }

    private void startVideoInBackground() {
        String videoPath = "android.resource://" + getPackageName() + "/" + R.raw.bg_android_i;

        videoView.setVideoPath(videoPath);
        videoView.start();
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoView.start();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (videoView != null && !videoView.isPlaying()) {
            videoView.start();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void setToolbarAndStatusBar() {
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            // getToolbar().setPadding(0, getStatusBarHeight(), 0, 0);

            if (Build.VERSION.SDK_INT >= 21) {
                setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
                getWindow().setStatusBarColor(Color.parseColor("#66000000"));
            } else {
                setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
            }
        }
    }

    private static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();

        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }

        win.setAttributes(winParams);
    }

    private void setupFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.d(LOGTAG, "Facebook login result success");
                        facebookAuth(loginResult.getAccessToken().getToken());
                    }

                    @Override
                    public void onCancel() {
                        Log.e(LOGTAG, "Facebook login cancelled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(LOGTAG, "Facebook login result error");
                    }
                });
    }

    private void facebookAuth(String fbToken) {
        fbLoginDialog.show();

        final Call<ApiResponse> loginCall = apiService.authFacebook(fbToken);
        loginCall.enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                ViewUtils.dismissDialog(fbLoginDialog);

                if (apiResponse.user != null) {
                    if (apiResponse.user.first_login) {
                        openSignupFragment(apiResponse.user);
                    } else {
                        if (apiResponse.user.idioms == null) {
                            openSelectIdiomFragment(apiResponse.user);
                        } else {
                            app.makeLogin(LoginActivity.this, apiResponse.user);
                        }
                    }
                } else {
                    onFailure(call, new Throwable("ApiResponse user is null"));
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                ViewUtils.dismissDialog(fbLoginDialog);
                String message = errorEnum.getErrorDescription(LoginActivity.this);
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openSignupFragment(User user) {
        Fragment currentFrag = getCurrentFragment();
        if (currentFrag instanceof SignupFragment) {
            ((SignupFragment) currentFrag).setUser(user);
        } else {
            SignupFragment signupFragment = SignupFragment_.builder().user(user).build();
            switchToFragment(signupFragment);
        }
    }

    private void openSelectIdiomFragment(User user) {
        SelectIdiomFragment selectIdiom = SelectIdiomFragment_.builder().user(user).isTeacher(true).build();

        if (logoToShare != null) {
            switchToFragmentWithSharedElementTransition(selectIdiom, logoToShare, tagForShare);
        } else {
            switchToFragment(selectIdiom);
        }
    }

    public void loginFacebook(View logo, String tag) {
        this.logoToShare = logo;
        this.tagForShare = tag;

        // User still logged in. Force logout.
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }

        String[] readPermissions = {"email", "user_birthday"};
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList(readPermissions));
    }

}