package club.idiom.app.activities;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.util.List;

import club.idiom.app.R;
import club.idiom.app.fragments.ChatListFragment_;
import club.idiom.app.fragments.FollowFragment_;
import club.idiom.app.fragments.FriendsListFragment_;
import club.idiom.app.models.User;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.utils.CometchatUtils;

@EActivity(R.layout.activity_root)
@OptionsMenu(R.menu.menu_root_requests)
public class RootActivity extends BaseActivity {

    @ViewById
    NavigationView navigationView;
    @ViewById
    Toolbar toolbar;
    @ViewById
    DrawerLayout drawerLayout;

    @OptionsMenuItem
    MenuItem requestsIcon;

    CometChat cometChat;
    long lastItemChecked;

    List<User> friendRequests;

    @AfterViews
    void init() {
        setSupportActionBar(toolbar);
        setupNavigationViewListener();
        synchronizeToobarAndDrawer();

        goToFirstFragment();

        cometChat = CometchatUtils.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupDrawerHeader();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void gotProfileInfoEvent(CometChatEvent.GotProfileInfoEvent event) {
        cometChat.getPluginInfo(new Callbacks() {
            @Override
            public void successCallback(JSONObject response) {
                Log.d("RootActivity", "Plugin information: " + response);
            }

            @Override
            public void failCallback(JSONObject response) {
                Log.e("RootActivity", "Plugin fail: " + response);
            }
        });
    }

    private void goToFirstFragment() {
        navigationView.setCheckedItem(R.id.tagsline);
        lastItemChecked = R.id.tagsline;
        switchToFragment(FriendsListFragment_.builder().build(), false);
    }

    private void synchronizeToobarAndDrawer() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        // Setting the actionbarToggle to drawer layout
        drawerLayout.removeDrawerListener(actionBarDrawerToggle);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        // Calling sync state is necessary, otherwise your hamburger icon won't show up
        actionBarDrawerToggle.syncState();
    }

    private void setupNavigationViewListener() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.getItemId() == lastItemChecked) {
                    drawerLayout.closeDrawers();
                    return true;
                }

                lastItemChecked = menuItem.getItemId();

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    case R.id.tagsline:
                        switchToFragment(FriendsListFragment_.builder().build());
                        return true;

                    case R.id.chat:
                        switchToFragment(ChatListFragment_.builder().build());
                        Toast.makeText(getApplicationContext(), "Chat Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.favorites:
                        switchToFragment(FollowFragment_.builder().build());
                        return true;

                    case R.id.events:
                        Toast.makeText(getApplicationContext(), "Events Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.settings:
                        Toast.makeText(getApplicationContext(), "Settings Selected", Toast.LENGTH_SHORT).show();
                        return true;

                    case R.id.logout:
                        app.makeLogout(RootActivity.this);
                        return true;

                    default:
                        Toast.makeText(getApplicationContext(), "Something is wrong", Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });
    }

    private void setupDrawerHeader() {
        View view = navigationView.getHeaderView(0);

        TextView name = (TextView) view.findViewById(R.id.username);
        TextView email = (TextView) view.findViewById(R.id.email);
        SimpleDraweeView avatar = (SimpleDraweeView) view.findViewById(R.id.avatar);

        User user = userConfig.getUser();
        name.setText(user.name);
        email.setText(user.email);
        view.setOnClickListener(getClickWithSharedElement(user));

        // Set the placeholder image to the placeholder vector drawable
        Drawable placeholderDrawable = ContextCompat.getDrawable(this, R.drawable.vector_person);
        avatar.getHierarchy().setPlaceholderImage(placeholderDrawable);
        avatar.setImageURI(Uri.parse(user.getAvatarUrl()));
    }


    View.OnClickListener getClickWithSharedElement(final User user) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowProfileActivity_.intent(RootActivity.this).user(user).start();
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}