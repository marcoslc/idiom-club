package club.idiom.app.activities;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import club.idiom.app.R;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.models.IdiomsEnum;
import club.idiom.app.models.User;
import club.idiom.app.models.UserIdioms;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

/**
 * Created by marcoscardoso on 26/03/16.
 */

@EActivity(R.layout.activity_show_profile)
@OptionsMenu(R.menu.menu_profile_edit)
public class ShowProfileActivity extends BaseActivity {
    public static final String LOGTAG = ShowProfileActivity.class.getSimpleName();

    @Extra
    User user;
    @ViewById
    Toolbar toolbar;
    @ViewById
    TextView usernameEt, emailEt;
    @ViewById
    Button loginBt;
    @ViewById
    SimpleDraweeView avatar, flagPtLearn, flagEsLearn, flagUsLearn, flagPtSpeak, flagEsSpeak, flagUsSpeak;
    @ViewById
    View callContainer, messageChat, videoChat, audioChat, addFriend;
    @ViewById
    RadioGroup genderRadioGroup;
    @ViewById
    TextView tml, hour, friends, labelFriend;

    @OptionsMenuItem
    MenuItem editProfile;

    ProgressDialog followUserDialog;

    IdiomApiCalback followUserCallback = new IdiomApiCalback() {
        @Override
        public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
            ViewUtils.dismissDialog(followUserDialog);
            user.following = true;
            setFollowingButton();
        }

        @Override
        public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
            ViewUtils.dismissDialog(followUserDialog);
            Log.e(LOGTAG, "followUser failed: " + errorEnum);
        }
    };

    IdiomApiCalback unfollowUserCallback = new IdiomApiCalback() {
        @Override
        public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
            ViewUtils.dismissDialog(followUserDialog);
            user.following = false;
            setFollowingButton();
        }

        @Override
        public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
            ViewUtils.dismissDialog(followUserDialog);
            Log.e(LOGTAG, "followUser failed: " + errorEnum);
        }
    };

    @AfterViews
    void init() {
        setupToolbar();

        followUserDialog = new ProgressDialog(this);
        followUserDialog.setIndeterminate(true);
        followUserDialog.setCancelable(false);
        followUserDialog.setMessage(getString(R.string.waiting));
    }

    protected void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        editProfile.setVisible(userConfig.getUser().id == user.id);
        addFriend.setVisibility(View.VISIBLE);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        populateUserData();
    }

    @UiThread
    protected void populateUserData() {
        if (userConfig.getUser().id == user.id) {
            user = userConfig.getUser();
        }

        usernameEt.setText(user.name);
        emailEt.setText(user.email);

        avatar.setImageURI(Uri.parse(user.getAvatarUrl()));

        int tmlI = (int) user.tml;

        String tmlS = String.format("%03d", tmlI);
        tml.setText(tmlS);

        if (user.id == userConfig.getUser().id) {
            //callContainer.setVisibility(View.GONE);
            videoChat.setVisibility(View.GONE);
            audioChat.setVisibility(View.GONE);
            messageChat.setVisibility(View.GONE);

        } else {
            //callContainer.setVisibility(View.VISIBLE);
            videoChat.setVisibility(View.VISIBLE);
            audioChat.setVisibility(View.VISIBLE);
            messageChat.setVisibility(View.VISIBLE);

        }

        setFlagVisibility(user.idioms, IdiomsEnum.pt_BR, flagPtLearn, flagPtSpeak);
        setFlagVisibility(user.idioms, IdiomsEnum.es_ES, flagEsLearn, flagEsSpeak);
        setFlagVisibility(user.idioms, IdiomsEnum.en_US, flagUsLearn, flagUsSpeak);

        setFollowingButton();
    }

    @UiThread
    public void setFollowingButton() {
        if (user.following) {
            labelFriend.setText(R.string.following_user);
            //iconFriend.setBackgroundResource(R.drawable.ic_done);
        } else {
            labelFriend.setText(R.string.follow_user);
            //iconFriend.setBackgroundResource(R.drawable.ic_plus);
        }
    }

    @OptionsItem
    void editProfile() {
        EditProfileActivity_.intent(this).user(user).start();
    }

    @Click
    void messageChatClicked() {
        ChatMessageActivity_.intent(this).user(user).start();
    }

    @Click
    void addFriendClicked() {
        if (user.following) {
            unfollowUser();
        } else {
            followUser();
        }
    }

    public void followUser() {
        final Call<ApiResponse> followUserCall = apiService.followUser(userConfig.getUser().token, String.valueOf(user.id));
        followUserDialog.show();
        followUserCall.enqueue(followUserCallback);
    }

    public void unfollowUser() {
        final Call<ApiResponse> unfollowUserCall = apiService.unfollowUser(userConfig.getUser().token, String.valueOf(user.id));
        followUserDialog.show();
        unfollowUserCall.enqueue(unfollowUserCallback);
    }

    @Click
    void videoChatClicked() {
        BeforeCallActivity_.intent(this).isReceiving(false).isVideo(true).user(user).start();
    }

    @Click
    void audioChatClicked() {
        BeforeCallActivity_.intent(this).isReceiving(false).isVideo(false).user(user).start();
    }

    @UiThread
    public void setFlagVisibility(UserIdioms idioms, IdiomsEnum idiomsEnum, View learnFlagView, View teachFlagView) {
        teachFlagView.setVisibility(idioms != null && idioms.teachesIdiom(idiomsEnum) ? View.VISIBLE : View.GONE);
        learnFlagView.setVisibility(idioms != null && idioms.learnsIdiom(idiomsEnum) ? View.VISIBLE : View.GONE);
    }
}