package club.idiom.app.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import club.idiom.app.R;
import club.idiom.app.adapters.IdiomSelectAdapter;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.application.MyApplication;
import club.idiom.app.models.User;
import club.idiom.app.models.UserIdioms;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.utils.CometchatUtils;
import club.idiom.app.utils.TakePictureUtil;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

/**
 * Created by marcoscardoso on 26/03/16.
 */

@EActivity(R.layout.activity_edit_profile)
public class EditProfileActivity extends BaseActivity {
    public static final String LOGTAG = EditProfileActivity.class.getSimpleName();
    private static final int PERMISSION_REQUEST_TAKE_PHOTO = 10;

    @Extra
    User user;
    @App
    MyApplication app;
    @ViewById
    Toolbar toolbar;
    @ViewById
    EditText usernameEt, birthdayEt, locationEt;
    @ViewById
    RadioGroup genderRadioGroup;

    @ViewById
    RecyclerView recyclerViewTeach, recyclerViewLearn;

    @ViewById
    SimpleDraweeView avatar;

    private IdiomSelectAdapter adapterTeach;
    private IdiomSelectAdapter adapterLearn;

    private ProgressDialog uploading;
    private long idPhotoUploaded = 0;
    private SimpleDateFormat sdf;

    @AfterViews
    void init() {
        setupToolbar();

        sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        populateFieldsWithUserData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initRecyclerViewLearn() {
        GridLayoutManager glm = new GridLayoutManager(this, 2);
        recyclerViewLearn.setLayoutManager(glm);
        recyclerViewLearn.setHasFixedSize(true);

        adapterLearn = new IdiomSelectAdapter(user.idioms.learn, true, true);

        recyclerViewLearn.setAdapter(adapterLearn);
    }

    private void initRecyclerViewTeach() {
        GridLayoutManager glm = new GridLayoutManager(this, 2);
        recyclerViewTeach.setLayoutManager(glm);
        recyclerViewTeach.setHasFixedSize(true);

        adapterTeach = new IdiomSelectAdapter(user.idioms.teach, true, true);
        recyclerViewTeach.setAdapter(adapterTeach);
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditProfileActivity.this.onBackPressed();
            }
        });
    }

    @Click
    void sendButtonClicked() {
        if (user.idioms == null) {
            user.idioms = new UserIdioms();
        }

        user.name = usernameEt.getText().toString();
        user.location = locationEt.getText().toString();

        switch (genderRadioGroup.getCheckedRadioButtonId()) {
            case R.id.male_radio:
                user.sex = "m";
                break;
            case R.id.female_radio:
                user.sex = "f";
                break;
        }

        try {
            Date date = sdf.parse(birthdayEt.getText().toString());
            user.born = date.getTime() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
        }

        user.timezone = TimeZone.getDefault().getID();
        user.idioms.teach = adapterTeach.getUserIdiomList();
        user.idioms.learn = adapterLearn.getUserIdiomList();

        Gson gson = ApiFactory.getGsonInstance();
        String json = gson.toJson(user);

        apiService.updateUser(user.token, json).enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                if (apiResponse.user != null) {
                    userConfig.saveUser(apiResponse.user);
                    finish();
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                showToast(R.string.error_unknown);
            }
        });
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void populateFieldsWithUserData() {
        if (user == null) {
            return;
        }

        if (user.born > 0) {
            // set calendar with date
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(user.born * 1000);

            // parse date and fill edit text
            String date = sdf.format(calendar.getTime());
            birthdayEt.setText(date);
        }

        usernameEt.setText(user.name);
        locationEt.setText(user.location);

        genderRadioGroup.clearCheck();
        genderRadioGroup.check("f".equals(user.sex) ? R.id.female_radio : R.id.male_radio);

        ViewUtils.setDrawee(avatar, getResources().getDimensionPixelSize(R.dimen.show_edit_avatar_size), user.getAvatarUrl());

        initRecyclerViewTeach();
        initRecyclerViewLearn();
    }

    @Click
    void avatarClicked() {
        checkPermissionsgetPhoto();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TakePictureUtil.onActivityResult(this, takePicture, requestCode, resultCode, data);
    }

    private TakePictureUtil.TakePictureCB takePicture = new TakePictureUtil.TakePictureCB() {
        @Override
        public void success(Bitmap image) {
            if (image == null) {
                Log.d(LOGTAG, "picture is null");
            } else {
                Log.d(LOGTAG, "got picture");
                sendPhoto(image);
            }
        }

        @Override
        public void error(Exception e) {
            Log.e(LOGTAG, "error getting picture", e);
        }
    };

    private void sendPhoto(final Bitmap image) {
        uploading = new ProgressDialog(this);
        uploading.setCancelable(false);
        uploading.setMessage(getResources().getString(R.string.uploading_foto));
        uploading.show();

        //Gambiarra Master pra enviar a foto e pegar a URL
        final CometChat cometChat = CometchatUtils.getInstance(this);
        cometChat.sendImage(image, String.valueOf(user.id), new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d(LOGTAG, jsonObject.toString());

                if (jsonObject.has("id")) {
                    try {
                        idPhotoUploaded = jsonObject.getLong("id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.d(LOGTAG, jsonObject.toString());
                uploading.dismiss();

            }
        });
    }

    private void checkPermissionsgetPhoto() {
        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])) {
                // Show an expanation to the user. After the user sees the explanation, try again to request the permission.
                showRationale(permissions);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_TAKE_PHOTO);
            }
        } else {
            onPermissionsGranted();
        }
    }

    public void showRationale(final String[] permissions) {
        new AlertDialog.Builder(this).setMessage(R.string.file_permission_rationale)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(EditProfileActivity.this, permissions, PERMISSION_REQUEST_TAKE_PHOTO);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onPermissionsDenied();
                    }
                })
                .show();
    }

    private void onPermissionsDenied() {
        showToast(R.string.file_permission_revoked);
    }

    private void onPermissionsGranted() {
        TakePictureUtil.startDialogGetImage(this);
    }

    @Subscribe
    public void onMessageReceived(CometChatEvent.OnMessageReceivedEvent event) {
        if (event.jsonObject != null) {
            Log.d(LOGTAG, "onMessageReceived " + event.jsonObject.toString());
            ChatMessage message = ChatMessage.fromMessageReceivedJson(event.jsonObject);

            if (message != null && message.getId() == idPhotoUploaded) {
                idPhotoUploaded = 0;
                if (uploading != null && uploading.isShowing())
                    uploading.dismiss();

                user.photo = message.getMessage();
                avatar.setImageURI(Uri.parse(user.getAvatarUrl()));
            }
        }
    }
}