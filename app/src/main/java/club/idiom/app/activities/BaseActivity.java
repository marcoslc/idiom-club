package club.idiom.app.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.ChangeBounds;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.TransitionSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONObject;

import club.idiom.app.R;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiService;
import club.idiom.app.application.MyApplication;
import club.idiom.app.singletons.MySharedPrefs_;
import club.idiom.app.singletons.UserConfig;
import club.idiom.app.utils.CometchatUtils;

/**
 * Created by Victor on 02/03/2016.
 */
@EActivity
public abstract class BaseActivity extends AppCompatActivity {
    private static final String LOGTAG = BaseActivity.class.getSimpleName();

    @App
    public MyApplication app;

    @Pref
    protected MySharedPrefs_ prefs;

    @Bean
    public UserConfig userConfig;

    public ApiService apiService;
    Fragment currentFragment;

    @AfterInject
    void afterInject() {
        apiService = ApiFactory.getApiService();
    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean isLoggedOnComet = CometChat.isLoggedIn();
        boolean isLoggedOnApp = userConfig.isLoggedIn();

        if (!isLoggedOnComet && isLoggedOnApp) {
            CometChat cometChat = CometchatUtils.getInstance(this);
            cometChat.setCometChatUrl(CometchatUtils.getCometchatUrl());

            cometChat.login(CometchatUtils.getCometchatUrl(), String.valueOf(userConfig.getUser().id), new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "cometchat login successful");
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "cometchat login failed: " + jsonObject);
                }
            });
        }
    }

    @UiThread
    public void showToast(int resourceId) {
        Toast.makeText(BaseActivity.this, resourceId, Toast.LENGTH_SHORT).show();
    }

    @UiThread
    public void showToast(String message) {
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * This method does not add in backstack
     *
     * @param newFrag
     */
    public void switchToFragment(@NonNull Fragment newFrag) {
        switchToFragment(newFrag, true);
    }

    public void switchToFragment(@NonNull Fragment newFrag, boolean addBackStack) {
        if (newFrag.equals(currentFragment)) {
            return;
        }

        try {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            transaction.replace(R.id.container_fragment, newFrag);

            if (addBackStack) {
                transaction.addToBackStack(newFrag.getClass().getName());
            }

            transaction.commit();
            currentFragment = newFrag;
            Log.d(LOGTAG, "Fragment " + newFrag.getClass().getName() + " commited");
        } catch (IllegalStateException e) {
            Log.e(LOGTAG, "Can't commit fragment: " + newFrag.getClass().getName(), e);
        }
    }

    public void switchToFragmentWithSharedElementTransition(@NonNull Fragment newFrag, View initial, String tag) {
        if (newFrag.equals(currentFragment)) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            newFrag.setSharedElementEnterTransition(new DetailsTransition());
            newFrag.setSharedElementReturnTransition(new DetailsTransition());
            newFrag.setEnterTransition(new Fade());

            final Fragment currentFragment = getCurrentFragment();
            if (currentFragment != null) {
                // currentFragment.setExitTransition(new Fade());
                currentFragment.setSharedElementReturnTransition(new DetailsTransition());
            }
        }

        final String fragmentName = newFrag.getClass().getName();
        getSupportFragmentManager()
                .beginTransaction()
                .addSharedElement(initial, tag)
                .replace(R.id.container_fragment, newFrag, fragmentName)
                .addToBackStack(fragmentName)
                .commitAllowingStateLoss();
        getSupportFragmentManager().executePendingTransactions();
        currentFragment = newFrag;
    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static class DetailsTransition extends TransitionSet {
        public DetailsTransition() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                setOrdering(ORDERING_TOGETHER);
                addTransition(new ChangeBounds());
                addTransition(new ChangeTransform());
                setDuration(200);
            }
        }
    }
}