package club.idiom.app.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.util.SortedList;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.OptionsMenuItem;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import club.idiom.app.R;
import club.idiom.app.adapters.ChatAdapter;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.models.User;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.models.events.CometChatEvent.OnMessageSentEvent;
import club.idiom.app.singletons.ChatPersist;
import club.idiom.app.utils.ActionsKeys;
import club.idiom.app.utils.CometchatUtils;
import club.idiom.app.utils.PushNotificationManager;
import club.idiom.app.utils.TakePictureUtil;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

/**
 * Created by marcoscardoso on 08/04/16.
 */

@EActivity(R.layout.activity_chat_message)
@OptionsMenu(R.menu.menu_chat_message)
public class ChatMessageActivity extends BaseActivity {
    private static final String LOGTAG = ChatMessageActivity.class.getSimpleName();
    private static final Long ALL_MESSAGES = -1l;
    private static final int MAX_OF_MESSAGES = 100;

    public static final int PERMISSION_REQUEST_FILES = 133;

    @ViewById
    RecyclerView recycler;
    @ViewById
    Toolbar toolbar;
    @ViewById
    EditText message;
    @ViewById
    View send;

    @OptionsMenuItem
    MenuItem actionFollow, actionUnfollow;

    @Extra
    User user;
    @Extra
    int notificationId = 0;

    @Bean
    public ChatPersist chatHistoryPersistence;

    private ChatAdapter adapter;
    private CometChat cometChat;

    ProgressDialog dialog;

    TakePictureUtil.TakePictureCB takePicture = new TakePictureUtil.TakePictureCB() {
        @Override
        public void success(Bitmap image) {
            if (image == null) {
                Log.d(LOGTAG, "picture is null");
            } else {
                Log.d(LOGTAG, "got picture");
                sendImage(image);
            }
        }

        @Override
        public void error(Exception e) {
            Log.e(LOGTAG, "error getting picture", e);
        }
    };

    IdiomApiCalback followUserCallback = new IdiomApiCalback() {
        @Override
        public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
            ViewUtils.dismissDialog(dialog);
            user.following = true;
            invalidateOptionsMenu();
        }

        @Override
        public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
            ViewUtils.dismissDialog(dialog);
            Log.e(LOGTAG, "followUser failed: " + errorEnum);
        }
    };

    IdiomApiCalback unfollowUserCallback = new IdiomApiCalback() {
        @Override
        public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
            ViewUtils.dismissDialog(dialog);
            user.following = false;
            invalidateOptionsMenu();
        }

        @Override
        public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
            ViewUtils.dismissDialog(dialog);
            Log.e(LOGTAG, "followUser failed: " + errorEnum);
        }
    };

    @AfterViews
    void init() {
        setupToolbar();
        setupRecycler();

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage(getString(R.string.waiting));

        cometChat = CometchatUtils.getInstance(this);
        fetchItemsFromPersistence();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        actionFollow.setVisible(!user.following);
        actionUnfollow.setVisible(user.following);

        return super.onPrepareOptionsMenu(menu);
    }

    @OptionsItem
    boolean actionFollow() {
        final Call<ApiResponse> followUserCall = apiService.followUser(userConfig.getUser().token, String.valueOf(user.id));
        dialog.show();
        followUserCall.enqueue(followUserCallback);
        return true;
    }

    @OptionsItem
    boolean actionUnfollow() {
        final Call<ApiResponse> unfollowUserCall = apiService.unfollowUser(userConfig.getUser().token, String.valueOf(user.id));
        dialog.show();
        unfollowUserCall.enqueue(unfollowUserCallback);
        return true;
    }

    private void dismissNotification() {
        prefs.getSharedPreferences().edit().remove(PushNotificationManager.CHAT_DESCRIPTION_TAG + user.id).apply();
        int notificationId = prefs.getSharedPreferences().getInt(PushNotificationManager.CHAT_TAG + user.id, 0);
        if (notificationId > 0)
            PushNotificationManager.cancelNotification(this, notificationId);

    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        prefs.edit().openChat().put(user.id).apply();
        fetchChatHistory();
    }

    @Override
    protected void onStop() {
        prefs.edit().openChat().put(0).apply();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onMessageReceived(CometChatEvent.OnMessageReceivedEvent messageReceived) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(messageReceived.jsonObject);

        if (message != null) {
            addMessage(message);
        }
        enableSendMessage(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnMessageSent(OnMessageSentEvent event) {
        ChatMessage chatMessage = ChatMessage.fromMessageSentJson(event.jsonObject);
        if (chatMessage != null) {
            adapter.add(chatMessage);
            recycler.scrollToPosition(adapter.getItemCount() - 1);
        }

        message.setText("");
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveItemsInPersistence();

    }

    private void saveItemsInPersistence() {
        SortedList<ChatMessage> listMessage = adapter.getList();
        ArrayList<ChatMessage> listToSave = new ArrayList<>(MAX_OF_MESSAGES);

        int initialPoint = 0;
        if (listMessage.size() > MAX_OF_MESSAGES) {
            initialPoint = listMessage.size() - MAX_OF_MESSAGES;
        }

        for (int i = initialPoint; i < listMessage.size(); i++) {
            listToSave.add(listMessage.get(i));
        }

        chatHistoryPersistence.saveChatHistoryById(user.id, listToSave);

        prefs.edit().openChat().put(0).apply();

    }

    private void fetchItemsFromPersistence() {
        List<ChatMessage> list = chatHistoryPersistence.getChatHistoryById(user.id);
        if (list != null && list.size() > 0) {
            adapter.getList().addAll(list);
        }

        scrollToBottom();
        dismissNotification();
    }

    public void fetchChatHistory() {
        cometChat.getChatHistory((long) user.id, ALL_MESSAGES, new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                if (jsonObject != null) {

                    try {
                        JSONArray array = jsonObject.getJSONArray("history");

                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);

                            ChatMessage message = ChatMessage.fromMessageReceivedJson(obj);
                            if (message != null && message.getMessage() != null && !message.getMessage().contains(ActionsKeys.ACTION)) {
                                addMessage(message);
                            }
                        }

                        scrollToBottom();
                        dismissNotification();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.e(LOGTAG, "getChatHistory failed: " + jsonObject);
            }
        });
    }

    private void scrollToBottom() {
        if (adapter.getItemCount() > 0) {
            recycler.scrollToPosition(adapter.getItemCount() - 1);
        }
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatMessageActivity.this.onBackPressed();
            }
        });

        getSupportActionBar().setTitle(user.name);
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void setupRecycler() {
        adapter = new ChatAdapter();

        recycler.setLayoutManager(new LinearLayoutManager(this));
        recycler.setAdapter(adapter);

        recycler.scrollToPosition(adapter.getItemCount() - 1);
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void enableSendMessage(boolean enable) {
        message.setEnabled(enable);
        send.setEnabled(enable);
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    void addMessage(ChatMessage message) {
        switch (message.getMessageType()) {
            case text:
            case image:
            case sound:
            case video:
            case file:
                adapter.add(message);
                recycler.scrollToPosition(adapter.getItemCount() - 1);
                break;
        }
    }

    @Click
    void sendClicked() {
        final String msg = message.getText().toString();
        if (TextUtils.isEmpty(msg)) {
            return;
        }

        enableSendMessage(false);

        cometChat.sendMessage(String.valueOf(user.id), msg, new Callbacks() {
            public void successCallback(JSONObject jsonObject) {
                Log.d(LOGTAG, "sendMessage success: " + jsonObject);
                EventBus.getDefault().post(new OnMessageSentEvent(jsonObject));
                enableSendMessage(true);
            }

            @Override
            public void failCallback(JSONObject jsonObject) {
                Log.e(LOGTAG, "sendMessage failure: " + jsonObject);
                enableSendMessage(true);
            }
        });
    }

    private void checkPermissionsAndSendImage() {
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])) {
                // Show an expanation to the user. After the user sees the explanation, try again to request the permission.
                showRationale(permissions);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_FILES);
            }
        } else {
            onImageFilePermissionsGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_FILES) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted! Do the task you need to do.
                onImageFilePermissionsGranted();
            } else {
                // Permission denied! Disable the functionality that depends on this permission.
                onImageFilePermissionsDenied();
            }
        }
    }

    public void showRationale(final String[] permissions) {
        new AlertDialog.Builder(this).setMessage(R.string.file_permission_rationale)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(ChatMessageActivity.this, permissions, PERMISSION_REQUEST_FILES);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onImageFilePermissionsDenied();
                    }
                })
                .show();
    }

    void onImageFilePermissionsGranted() {
        TakePictureUtil.startDialogGetImage(this);
    }

    void onImageFilePermissionsDenied() {
        showToast(R.string.file_permission_revoked);
    }

    @Click
    void photoBtClicked() {
        checkPermissionsAndSendImage();
    }

    @OptionsItem
    public void actionVideoCallSelected() {
        new AlertDialog.Builder(this).setMessage(R.string.receiving_call)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BeforeCallActivity_.intent(ChatMessageActivity.this)
                                .isReceiving(false)
                                .user(user)
                                .isVideo(true)
                                .start();
                    }
                }).setNegativeButton(android.R.string.no, null).show();
    }


    @OptionsItem
    public void actionAudioCallSelected() {
        new AlertDialog.Builder(this).setMessage(R.string.receiving_audio_call)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BeforeCallActivity_.intent(ChatMessageActivity.this)
                                .isReceiving(false)
                                .user(user)
                                .isVideo(false)
                                .start();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        TakePictureUtil.onActivityResult(this, takePicture, requestCode, resultCode, data);
    }

    private void sendImage(Bitmap bitmap) {
        cometChat.sendImage(bitmap, String.valueOf(user.id), new Callbacks() {
            @Override
            public void successCallback(JSONObject jsonObject) {
                Log.d("ChatMessageActivity", jsonObject.toString());
            }

            @Override
            public void failCallback(JSONObject jsonObject) {

            }
        });
    }
}