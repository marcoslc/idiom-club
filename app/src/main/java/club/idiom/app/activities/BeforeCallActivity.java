package club.idiom.app.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.inscripts.cometchat.sdk.AVChat;
import com.inscripts.cometchat.sdk.AudioChat;
import com.inscripts.cometchat.sdk.CometChat;
import com.inscripts.interfaces.Callbacks;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.api.BackgroundExecutor;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import club.idiom.app.R;
import club.idiom.app.models.User;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.events.CometChatEvent.OnAVChatMessageReceivedEvent;
import club.idiom.app.utils.CometchatUtils;

/**
 * Created by marcoscardoso on 08/04/16.
 */

@EActivity(R.layout.activity_chat_call_received)
public class BeforeCallActivity extends BaseActivity {
    private static final String LOGTAG = BeforeCallActivity.class.getSimpleName();
    public final int VIBRATE_ACTIVE_TIME = 1000;
    public final int VIBRATE_INACTIVE_TIME = 2000;

    private static final int PERMISSION_REQUEST_CAMERA_AUDIO = 357;

    @ViewById
    SimpleDraweeView avatar;
    @ViewById
    TextView nameUser, description, location;
    @ViewById
    Button acceptBt, cancelBt, rejectBt;

    @Extra
    boolean isReceiving;
    @Extra
    User user;
    @Extra
    String callId;

    @Extra
    boolean isVideo;

    @SystemService
    Vibrator vibrator;

    CometChat cometChat;
    AVChat avChat;
    AudioChat audioChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @AfterViews
    void init() {
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = 1;
        getWindow().setAttributes(params);

        if (isReceiving) {
            if (isVideo) {
                description.setText(R.string.receiving_call_description_video);
            } else {
                description.setText(R.string.receiving_call_description_audio);
            }

            cancelBt.setVisibility(View.GONE);
            acceptBt.setVisibility(View.VISIBLE);
            rejectBt.setVisibility(View.VISIBLE);
        } else {
            description.setText(R.string.calling);
            cancelBt.setVisibility(View.VISIBLE);
            acceptBt.setVisibility(View.GONE);
            rejectBt.setVisibility(View.GONE);
        }

        cometChat = CometchatUtils.getInstance(this);
        avChat = AVChat.getAVChatInstance(this);
        audioChat = AudioChat.getInstance(this);
        nameUser.setText(user.name);

        String locationAndDate = TextUtils.isEmpty(user.location) ? getString(R.string.location_unknown) : user.location;

        if (!TextUtils.isEmpty(user.timezone)) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.US);
            df.setTimeZone(TimeZone.getTimeZone(user.timezone)); //format in given timezone
            String strDate = df.format(new Date());
            locationAndDate += "\n" + strDate;
        }

        location.setText(locationAndDate);
        setAvatar();

        checkPermissionsAndStartCall();
    }

    private void checkPermissionsAndStartCall() {
        String[] permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WAKE_LOCK};

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[1])) {
                // Show an expanation to the user. After the user sees the explanation, try again to request the permission.
                showRationale(permissions);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CAMERA_AUDIO);
            }
        } else {
            onPermissionsGranted();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CAMERA_AUDIO) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults.length == permissions.length
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted! Do the task you need to do.
                onPermissionsGranted();
            } else {
                // Permission denied! Disable the functionality that depends on this permission.
                onPermissionsDenied();
            }
        }
    }

    public void showRationale(final String[] permissions) {
        new AlertDialog.Builder(this).setMessage(R.string.av_permission_rationale)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(BeforeCallActivity.this, permissions, PERMISSION_REQUEST_CAMERA_AUDIO);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onPermissionsDenied();
                    }
                })
                .show();
    }

    void onPermissionsGranted() {
        if (!isReceiving) {
            sendAVChatRequest();
        }
    }

    void onPermissionsDenied() {
        finish();
    }

    @Background(id = "vibrate_task", delay = VIBRATE_INACTIVE_TIME)
    void waitAndVibrate() {
        vibrator.vibrate(VIBRATE_ACTIVE_TIME);
        waitAndVibrate();
    }

    public void sendAVChatRequest() {
        if (isVideo) {
            avChat = AVChat.getAVChatInstance(this);
            avChat.sendAVChatRequest(String.valueOf(user.id), new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "sendAVChatRequest success: " + jsonObject);
                    try {
                        if (jsonObject.has("callid"))
                            callId = jsonObject.getString("callid");
                    } catch (Exception e) {
                        e.printStackTrace();
                        showToast(R.string.call_error);
                        finish();
                    }
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "sendAVChatRequest fail: " + jsonObject);
                    showToast(R.string.call_error);
                    finish();
                }
            });
        } else {
            audioChat = AudioChat.getInstance(this);
            audioChat.sendAudioChatRequest(String.valueOf(user.id), new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                    Log.d(LOGTAG, "sendAudioChatRequest success: " + jsonObject);

                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                    Log.e(LOGTAG, "sendAudioChatRequest fail: " + jsonObject);
                    showToast(R.string.call_error);
                    finish();
                }
            });
        }
    }

    private void startCall(boolean isVideo) {
        CallAudioVideoActivity_.intent(BeforeCallActivity.this)
                .user(user)
                .isReceiving(isReceiving)
                .callId(callId)
                .isVideo(isVideo)
                .start();
        finish();
    }

    @Subscribe
    public void OnAVChatMessageReceived(OnAVChatMessageReceivedEvent event) {
        ChatMessage message = ChatMessage.fromMessageReceivedJson(event.jsonObject);

        if (message != null && message.getFrom() == user.id) {
            switch (message.getMessageType()) {
                case accepted_call:
                    if (message.getCallid() != null && !"".equals(message.getCallid()))
                        callId = message.getCallid();
                    startCall(isVideo);
                    break;

                case rejected_call:
                    showToast(R.string.rejected_call);
                    finish();
                    break;

                case cancelled_call:
                    showToast(R.string.cancelled_call);
                    finish();
                    break;

                case timeout_call:
                    showToast(R.string.timeout_call);
                    finish();
                    break;

                case destinatary_busy_call:
                    showToast(R.string.destinatary_busy_call);
                    finish();
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isReceiving) {
            vibrator.vibrate(VIBRATE_ACTIVE_TIME);
            waitAndVibrate();
        }
    }

    @Override
    protected void onPause() {
        BackgroundExecutor.cancelAll("vibrate_task", true);
        super.onPause();
    }

    void setAvatar() {
        avatar.setImageURI(Uri.parse(user.getAvatarUrl()));
    }

    @UiThread
    public void acceptCall() {
        final ProgressDialog accepting = new ProgressDialog(this);
        accepting.setMessage("Accepting");
        accepting.show();

        if (isVideo) {
            avChat.acceptAVChatRequest(String.valueOf(user.id), new Callbacks() {
                @Override
                public void successCallback(JSONObject response) {
                    Log.d(LOGTAG, "acceptAVChatRequest success: " + response);

                    startCall(true);

                    if (accepting.isShowing()) {
                        accepting.dismiss();
                    }
                }

                @Override
                public void failCallback(JSONObject response) {
                    Log.e(LOGTAG, "acceptAVChatRequest fail: " + response);
                    finish();
                }
            });
        } else {
            audioChat.acceptAudioChatRequest(String.valueOf(user.id), new Callbacks() {
                @Override
                public void successCallback(JSONObject response) {
                    Log.d(LOGTAG, "acceptAVChatRequest success: " + response);

                    startCall(false);

                    if (accepting.isShowing()) {
                        accepting.dismiss();
                    }
                }

                @Override
                public void failCallback(JSONObject response) {
                    Log.e(LOGTAG, "acceptAVChatRequest fail: " + response);
                    finish();
                }
            });
        }
    }

    private void rejectCall() {

        if (isVideo) {
            avChat.rejectAVChatRequest(String.valueOf(user.id), callId, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                }
            });
        } else {
            audioChat.rejectAudioChatRequest(String.valueOf(user.id), callId, new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {
                }

                @Override
                public void failCallback(JSONObject jsonObject) {
                }
            });
        }
    }

    private void cancelCall() {

        if (isVideo) {
            avChat = AVChat.getAVChatInstance(this);
            avChat.cancelAVChatRequest(String.valueOf(user.id), new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {

                }

                @Override
                public void failCallback(JSONObject jsonObject) {

                }
            });
        } else {
            audioChat = AudioChat.getInstance(this);
            audioChat.cancelAudioChatRequest(String.valueOf(user.id), new Callbacks() {
                @Override
                public void successCallback(JSONObject jsonObject) {

                }

                @Override
                public void failCallback(JSONObject jsonObject) {

                }
            });
        }

        finish();
    }

    @Click
    void acceptBtClicked() {
        acceptCall();
    }

    @Click
    void rejectBtClicked() {
        rejectCall();
        finish();
    }

    @Click
    void cancelBtClicked() {
        cancelCall();
    }
}