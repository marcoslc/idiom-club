package club.idiom.app.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import club.idiom.app.R;
import club.idiom.app.fragments.FriendsListFragment;
import club.idiom.app.fragments.FriendsListFragment_;

/**
 * Created by marcoscardoso on 05/08/16.
 */

public class FollowPageAdapter extends FragmentStatePagerAdapter {
    final int PAGE_COUNT = 3;

    private String tabTitles[];
    private Context context;

    public FollowPageAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

        String follower = context.getResources().getString(R.string.follower);
        String following = context.getResources().getString(R.string.following);
        String blocked = context.getResources().getString(R.string.blocked);

        tabTitles = new String[]{follower, following, blocked};
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;

        switch (position) {
            case 0:
                frag = FriendsListFragment_.builder()
                        .typeList(FriendsListFragment.TypeListFollow.FOLLOWERS)
                        .FILTER(FriendsListFragment.FRIENDS_FILTER)
                        .build();
                break;

            case 1:
                frag = FriendsListFragment_.builder()
                        .typeList(FriendsListFragment.TypeListFollow.FOLLOWING)
                        .FILTER(FriendsListFragment.FRIENDS_FILTER)
                        .build();
                break;

            case 2:
                frag = FriendsListFragment_.builder()
                        .typeList(FriendsListFragment.TypeListFollow.BLOCKED)
                        .FILTER(FriendsListFragment.FRIENDS_FILTER)
                        .build();
                break;
        }

        return frag;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}