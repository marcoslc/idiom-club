package club.idiom.app.adapters.viewholders;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;

import club.idiom.app.R;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.utils.ViewUtils;

/**
 * Created by Victor on 23/03/2016.
 */
public class ChatVideoViewHolder extends ChatBaseViewHolder {
    SimpleDraweeView image;

    public ChatVideoViewHolder(View view) {
        super(view);

        image = (SimpleDraweeView) itemView.findViewById(R.id.image);
    }

    public void setupView(ChatMessage item) {
        super.setupView(item);

        setImage(item);
    }

    void setImage(final ChatMessage chatMessage) {
        int size = itemView.getResources().getDimensionPixelSize(R.dimen.chat_row_image_size);
        ViewUtils.setDrawee(image, size, chatMessage.getMessage() == null ? "" : chatMessage.getMessage());

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("teste","click");
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(chatMessage.getMessage()));
                intent.setDataAndType(Uri.parse(chatMessage.getMessage()), "video/*");
                image.getContext().startActivity(intent);
            }
        });
    }
}