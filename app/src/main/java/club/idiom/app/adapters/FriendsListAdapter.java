package club.idiom.app.adapters;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import club.idiom.app.R;
import club.idiom.app.adapters.viewholders.FriendViewHolder;
import club.idiom.app.models.User;

/**
 * Created by Victor on 23/03/2016.
 */
public class FriendsListAdapter extends RecyclerView.Adapter<FriendViewHolder> {

    SortedList<User> mList;

    public FriendsListAdapter() {
        mList = new SortedList<>(User.class, new SortedList.Callback<User>() {
            @Override
            public int compare(User o1, User o2) {
                return ((int) o1.lastactivity) - ((int) o2.lastactivity);
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(User oldItem, User newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(User item1, User item2) {
                return item1.id == item2.id;
            }
        });
    }

    public void add(List<User> items) {
        int start = mList.size();
        mList.addAll(items);
        notifyItemRangeInserted(start, items.size());
    }

    public void clear() {
        int count = mList.size();
        mList.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_friend, parent, false);
        FriendViewHolder holder = new FriendViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        User user = mList.get(position);
        holder.setupView(user);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}