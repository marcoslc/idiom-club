package club.idiom.app.adapters.viewholders;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import club.idiom.app.R;
import club.idiom.app.activities.ShowProfileActivity_;
import club.idiom.app.models.IdiomsEnum;
import club.idiom.app.models.User;
import club.idiom.app.models.UserIdioms;
import club.idiom.app.utils.ViewUtils;

/**
 * Created by Victor on 23/03/2016.
 */
public class FriendViewHolder extends RecyclerView.ViewHolder {

    private static final String AVAILABLE_STATUS = "available";
    private static final String BUSY_STATUS = "busy";

    SimpleDraweeView avatar, learnPT, learnES, learnUS, speakPT, speakES, speakUS;
    TextView name, location;
    ImageView statusImg;

    public FriendViewHolder(View itemView) {
        super(itemView);

        avatar = (SimpleDraweeView) itemView.findViewById(R.id.avatar);
        name = (TextView) itemView.findViewById(R.id.name);
        location = (TextView) itemView.findViewById(R.id.location);

        learnPT = (SimpleDraweeView) itemView.findViewById(R.id.flag_pt_learn);
        learnES = (SimpleDraweeView) itemView.findViewById(R.id.flag_es_learn);
        learnUS = (SimpleDraweeView) itemView.findViewById(R.id.flag_us_learn);

        speakPT = (SimpleDraweeView) itemView.findViewById(R.id.flag_pt_speak);
        speakES = (SimpleDraweeView) itemView.findViewById(R.id.flag_es_speak);
        speakUS = (SimpleDraweeView) itemView.findViewById(R.id.flag_us_speak);

        statusImg = (ImageView) itemView.findViewById(R.id.statusImg);
    }

    public void setupView(final User user) {
        name.setText(user.name);
        location.setText("Durham, NC");

        setAvatar(user);

        setFlagVisibility(user.idioms, IdiomsEnum.pt_BR, learnPT, speakPT);
        setFlagVisibility(user.idioms, IdiomsEnum.es_ES, learnES, speakES);
        setFlagVisibility(user.idioms, IdiomsEnum.en_US, learnUS, speakUS);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowProfileActivity_
                        .intent(itemView.getContext())
                        .user(user)
                        .start();
            }
        });


        Log.v("FriendViewHolder", user.name + " -> " + user.status);
        setStatus(statusImg, user.status);
    }

    private void setStatus(ImageView statusImg, String status) {

        Resources res = statusImg.getContext().getResources();

        if (AVAILABLE_STATUS.equals(status)) {
            statusImg.setBackground(res.getDrawable(R.drawable.background_available_status));
        } else if (BUSY_STATUS.equals(status)) {
            statusImg.setBackground(res.getDrawable(R.drawable.background_busy_status));
        } else {
            statusImg.setBackground(res.getDrawable(R.drawable.background_offline_status));

        }
    }

    public void setFlagVisibility(UserIdioms idioms, IdiomsEnum idiomsEnum, View learnFlagView, View teachFlagView) {
        teachFlagView.setVisibility(idioms != null && idioms.teachesIdiom(idiomsEnum) ? View.VISIBLE : View.GONE);
        learnFlagView.setVisibility(idioms != null && idioms.learnsIdiom(idiomsEnum) ? View.VISIBLE : View.GONE);
    }

    void setAvatar(User user) {
        int size = itemView.getResources().getDimensionPixelSize(R.dimen.friend_avatar_width);
        ViewUtils.setDrawee(avatar, size, user.getAvatarUrl());
    }
}