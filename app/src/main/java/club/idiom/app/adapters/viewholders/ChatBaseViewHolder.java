package club.idiom.app.adapters.viewholders;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import club.idiom.app.R;
import club.idiom.app.models.chatmodel.ChatMessage;
import tk.zielony.naturaldateformat.AbsoluteDateFormat;
import tk.zielony.naturaldateformat.NaturalDateFormat;

/**
 * Created by Victor on 23/03/2016.
 */
public class ChatBaseViewHolder extends RecyclerView.ViewHolder {
    TextView time, state;
    LinearLayout root;

    int padBotTop, padSide, padSenderSide;

    public ChatBaseViewHolder(View view) {
        super(view);

        root = (LinearLayout) itemView.findViewById(R.id.root);
        time = (TextView) itemView.findViewById(R.id.time);
        state = (TextView) itemView.findViewById(R.id.state);

        Resources res = itemView.getContext().getResources();
        padBotTop = res.getDimensionPixelSize(R.dimen.chat_row_margin_top_bottom);
        padSide = res.getDimensionPixelSize(R.dimen.chat_row_margin_side);
        padSenderSide = res.getDimensionPixelSize(R.dimen.chat_row_margin_side_sent);
    }

    public void setupView(ChatMessage item) {

        if (item.isFromMyself()) {
            root.setGravity(Gravity.RIGHT);
            root.setPadding(padSenderSide, padBotTop, padSide, padBotTop);
        } else {
            root.setGravity(Gravity.LEFT);
            root.setPadding(padSide, padBotTop, padSenderSide, padBotTop);
        }

        if (item.getSentTimestamp() == 0) {
            time.setText("");
        } else {
            AbsoluteDateFormat dateFormat = new AbsoluteDateFormat(itemView.getContext(), NaturalDateFormat.HOURS | NaturalDateFormat.MINUTES);
            time.setText(dateFormat.format(item.getSentTimestamp()));
        }

//        state.setText("" + item.getState() + (item.getState() == ChatStateEnum.sending ? "..." : ""));
    }
}