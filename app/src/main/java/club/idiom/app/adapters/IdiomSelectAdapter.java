package club.idiom.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import club.idiom.app.R;
import club.idiom.app.adapters.viewholders.IdiomViewHolder;
import club.idiom.app.models.IdiomsEnum;
import club.idiom.app.models.UserIdiom;

/**
 * Created by Victor on 07/04/2016.
 */

public class IdiomSelectAdapter extends RecyclerView.Adapter<IdiomViewHolder> {
    private ArrayList<IdiomsEnum> allIdiomsList;
    private List<UserIdiom> userIdiomList;
    private boolean isDarkTheme, allowEditing;

    public IdiomSelectAdapter(List<UserIdiom> userIdiomList, boolean isDarkTheme, boolean allowEditing) {
        this.isDarkTheme = isDarkTheme;
        this.allowEditing = allowEditing;
        allIdiomsList = new ArrayList<>();
        this.userIdiomList = new ArrayList<>();
        if (userIdiomList != null) {
            this.userIdiomList.addAll(userIdiomList);
        }

        // populate list with all enums != unknown
        for (IdiomsEnum idiom : IdiomsEnum.values()) {
            if (!IdiomsEnum.unknown.equals(idiom)) {
                allIdiomsList.add(idiom);
            }
        }
    }

    @Override
    public IdiomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_idiom_select, parent, false);
        IdiomViewHolder holder = new IdiomViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(IdiomViewHolder holder, int position) {
        IdiomsEnum idiomEnum = allIdiomsList.get(position);
        holder.setupView(idiomEnum, userIdiomList, isDarkTheme, allowEditing);
    }

    @Override
    public int getItemCount() {
        return allIdiomsList.size();
    }

    public List<UserIdiom> getUserIdiomList() {
        return userIdiomList;
    }
}