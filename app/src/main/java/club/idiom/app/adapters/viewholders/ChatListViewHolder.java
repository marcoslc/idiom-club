package club.idiom.app.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import club.idiom.app.R;
import club.idiom.app.activities.ChatMessageActivity_;
import club.idiom.app.models.User;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.utils.ViewUtils;

/**
 * Created by Victor on 28/04/2016.
 */
public class ChatListViewHolder extends RecyclerView.ViewHolder {
    SimpleDraweeView avatar;
    TextView name, lastMessage;

    public ChatListViewHolder(View itemView) {
        super(itemView);

        avatar = (SimpleDraweeView) itemView.findViewById(R.id.avatar);
        name = (TextView) itemView.findViewById(R.id.name);
        lastMessage = (TextView) itemView.findViewById(R.id.last_message);
    }

    public void setupView(final User user, ChatMessage message) {
        name.setText(user.name);
        lastMessage.setText(message.getMessage());

        int size = itemView.getResources().getDimensionPixelSize(R.dimen.friend_avatar_width);
        ViewUtils.setDrawee(avatar, size, user.getAvatarUrl());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChatMessageActivity_.intent(itemView.getContext()).user(user).start();
            }
        });
    }
}