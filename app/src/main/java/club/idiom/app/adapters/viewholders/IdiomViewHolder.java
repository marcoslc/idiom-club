package club.idiom.app.adapters.viewholders;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import club.idiom.app.R;
import club.idiom.app.models.IdiomsEnum;
import club.idiom.app.models.UserIdiom;

/**
 * Created by Victor on 07/04/2016.
 */
public class IdiomViewHolder extends RecyclerView.ViewHolder {
    TextView name;
    ImageView flag;
    View background;

    public IdiomViewHolder(View itemView) {
        super(itemView);

        background = itemView.findViewById(R.id.bg);
        name = (TextView) itemView.findViewById(R.id.idiom_name);
        flag = (ImageView) itemView.findViewById(R.id.idiom_flag);
    }

    public void setupView(final IdiomsEnum idiomEnum, final List<UserIdiom> userIdiomList, final boolean isDarkTheme, final boolean allowEditing) {
        name.setTextColor(ContextCompat.getColor(itemView.getContext(), isDarkTheme ? R.color.black : R.color.white));
        name.setText(idiomEnum.getIdiomName(itemView.getContext()));

        Drawable flagDrawable = ContextCompat.getDrawable(itemView.getContext(), idiomEnum.getIdiomFlag());
        flag.setImageDrawable(flagDrawable);

        final boolean userHasIdiom = userHasIdiom(idiomEnum, userIdiomList);
        background.setBackgroundResource(userHasIdiom
                ? (isDarkTheme ? R.drawable.black_border_bg : R.drawable.button_fill_bg)
                : 0);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserIdiom idiom = findIdiomInUserList(idiomEnum, userIdiomList);

                if (idiom == null) {
                    // Idiom not present in the list. Add it.
                    UserIdiom newIdiom = new UserIdiom(idiomEnum.getValue(), idiomEnum.getIdiomName(itemView.getContext()));
                    userIdiomList.add(newIdiom);
                } else {
                    // Idiom is in the list. Remove it.
                    userIdiomList.remove(idiom);
                }

                // Refresh view
                setupView(idiomEnum, userIdiomList, isDarkTheme, allowEditing);
            }
        };

        itemView.setOnClickListener(allowEditing ? clickListener : null);
    }

    public static UserIdiom findIdiomInUserList(IdiomsEnum idiomEnum, List<UserIdiom> userIdiomList) {
        for (UserIdiom idiom : userIdiomList) {
            if (idiom.id == idiomEnum.getValue()) {
                return idiom;
            }
        }

        return null;
    }

    public static boolean userHasIdiom(IdiomsEnum idiomEnum, List<UserIdiom> userIdiomList) {
        return findIdiomInUserList(idiomEnum, userIdiomList) != null;
    }
}