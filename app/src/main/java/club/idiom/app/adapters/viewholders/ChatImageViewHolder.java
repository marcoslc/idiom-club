package club.idiom.app.adapters.viewholders;

import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;

import club.idiom.app.R;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.utils.ViewUtils;

/**
 * Created by Victor on 23/03/2016.
 */
public class ChatImageViewHolder extends ChatBaseViewHolder {
    SimpleDraweeView image;

    public ChatImageViewHolder(View view) {
        super(view);

        image = (SimpleDraweeView) itemView.findViewById(R.id.image);
    }

    public void setupView(ChatMessage item) {
        super.setupView(item);

        setImage(item);
    }

    void setImage(ChatMessage chatMessage) {
        int size = itemView.getResources().getDimensionPixelSize(R.dimen.chat_row_image_size);
        ViewUtils.setDrawee(image, size, chatMessage.getMessage() == null ? "" : chatMessage.getMessage());
    }
}