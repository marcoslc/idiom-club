package club.idiom.app.adapters;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import club.idiom.app.R;
import club.idiom.app.adapters.viewholders.ChatBaseViewHolder;
import club.idiom.app.adapters.viewholders.ChatImageViewHolder;
import club.idiom.app.adapters.viewholders.ChatMessageViewHolder;
import club.idiom.app.adapters.viewholders.ChatVideoViewHolder;
import club.idiom.app.models.chatmodel.ChatMessage;
import club.idiom.app.models.chatmodel.ChatTypeEnum;

/**
 * Created by Victor on 23/03/2016.
 */
public class ChatAdapter extends RecyclerView.Adapter<ChatBaseViewHolder> {
    SortedList<ChatMessage> list;

    public ChatAdapter() {
        list = new SortedList<>(ChatMessage.class, new SortedList.Callback<ChatMessage>() {

            @Override
            public int compare(ChatMessage o1, ChatMessage o2) {
                return o1.getId() - o2.getId();
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(ChatMessage oldItem, ChatMessage newItem) {
                return oldItem.equals(newItem);
            }

            @Override
            public boolean areItemsTheSame(ChatMessage item1, ChatMessage item2) {
                return item1.getId() == item2.getId();
            }
        });
    }

    public void add(ChatMessage chatItem) {
        list.add(chatItem);
    }

    public void remove(ChatMessage chatItem){
        list.remove(chatItem);
    }

    @Override
    public int getItemViewType(int position) {
        list.addAll();
        return list.get(position).getMessageType().getValue();
    }

    @Override
    public ChatBaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ChatBaseViewHolder holder;
        ChatTypeEnum type = ChatTypeEnum.getTypeForInt(viewType);

        switch (type) {
            case text:
                View text = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_message, parent, false);
                holder = new ChatMessageViewHolder(text);
                break;

            case image:
                View image = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_image, parent, false);
                holder = new ChatImageViewHolder(image);
                break;

            case sound:
            case video:
                View video = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_image, parent, false);
                holder = new ChatVideoViewHolder(video);
                break;

            case unknown:
            default:
                View unknown = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_message, parent, false);
                holder = new ChatMessageViewHolder(unknown);
                break;
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(ChatBaseViewHolder holder, int position) {
        ChatMessage item = list.get(position);
        holder.setupView(item);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public SortedList<ChatMessage> getList() {
        return list;
    }
}