package club.idiom.app.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import club.idiom.app.R;
import club.idiom.app.adapters.viewholders.ChatListViewHolder;
import club.idiom.app.singletons.LastChatsInfo.ChatInfo;

/**
 * Created by Victor on 28/04/2016.
 */
public class ChatsListAdapter extends RecyclerView.Adapter<ChatListViewHolder> implements Comparator<ChatInfo> {
    ArrayList<ChatInfo> lastChats = new ArrayList<>();

    public void setChatList(ArrayList<ChatInfo> list) {
        this.lastChats = list;
        Collections.sort(lastChats, this);
        notifyDataSetChanged();
    }

    @Override
    public int compare(ChatInfo lhs, ChatInfo rhs) {
        return rhs.lastMessage.getId() - lhs.lastMessage.getId();
    }

    @Override
    public ChatListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_list, parent, false);
        return new ChatListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatListViewHolder holder, int position) {
        ChatInfo chatInfo = lastChats.get(position);
        holder.setupView(chatInfo.user, chatInfo.lastMessage);
    }

    @Override
    public int getItemCount() {
        return lastChats.size();
    }
}