package club.idiom.app.adapters.viewholders;

import android.view.View;
import android.widget.TextView;

import club.idiom.app.R;
import club.idiom.app.models.chatmodel.ChatMessage;

/**
 * Created by Victor on 23/03/2016.
 */
public class ChatMessageViewHolder extends ChatBaseViewHolder {
    TextView message;

    public ChatMessageViewHolder(View view) {
        super(view);

        message = (TextView) itemView.findViewById(R.id.message);
    }

    public void setupView(ChatMessage chatMessage) {
        super.setupView(chatMessage);

        message.setText(chatMessage.getMessage());
    }

}