package club.idiom.app.fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import club.idiom.app.R;
import club.idiom.app.activities.LoginActivity;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ApiService;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import retrofit2.Call;

/**
 * Created by marcoscardoso on 05/08/16.
 */

@EFragment(R.layout.fragment_forget_password)
public class ForgetPasswordFragment extends Fragment {

    public ApiService apiService;

    @ViewById
    EditText emailEt;

    @ViewById
    ImageView logoWhite;
    private ProgressDialog progressDialog;

    @AfterInject
    void afterInject() {
        apiService = ApiFactory.getApiService();
    }

    @AfterViews
    void init() {
        ViewCompat.setTransitionName(logoWhite, "logo");
    }

    @Click
    void sendBtClicked() {
       String email = emailEt.getText().toString();
        if (TextUtils.isEmpty(email)) {
            return;
        }

        showDialogWaitingRequest();

        Call<ApiResponse> forgotPassCall = apiService.forgotPassword(email);
        forgotPassCall.enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                if (progressDialog != null)
                    progressDialog.dismiss();

                Toast.makeText(getActivity(), R.string.forgot_pass_success, Toast.LENGTH_LONG).show();

                if (getActivity() instanceof LoginActivity) {
                    ((LoginActivity) getActivity()).switchToFragment(PinForgetPasswordFragment_.builder().build());
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                if (progressDialog != null)
                    progressDialog.dismiss();

                Log.e(LOGTAG, "forgotPassword failed: " + errorEnum.getErrorDescription(getActivity()));
                Toast.makeText(getActivity(), R.string.error_unknown, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showDialogWaitingRequest() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getResources().getString(R.string.redefining_password));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }
}
