package club.idiom.app.fragments;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import club.idiom.app.R;
import club.idiom.app.adapters.FollowPageAdapter;

/**
 * Created by marcoscardoso on 05/08/16.
 */

@EFragment(R.layout.fragment_follow)
public class FollowFragment extends Fragment {

    @ViewById
    ViewPager viewpager;

    @ViewById
    TabLayout slidingTabs;

    FollowPageAdapter adapter;

    @AfterViews
    void init() {
        adapter = new FollowPageAdapter(getActivity().getSupportFragmentManager(), getActivity());
        viewpager.setAdapter(adapter);
        slidingTabs.setupWithViewPager(viewpager);
    }
}