package club.idiom.app.fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import club.idiom.app.R;
import club.idiom.app.activities.BaseActivity;
import club.idiom.app.adapters.IdiomSelectAdapter;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ApiService;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.application.MyApplication;
import club.idiom.app.models.User;
import club.idiom.app.models.UserIdiom;
import club.idiom.app.models.UserIdioms;
import club.idiom.app.singletons.UserConfig;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

@EFragment(R.layout.fragment_select_idiom)
public class SelectIdiomFragment extends Fragment {
    public static String LOGTAG = SelectIdiomFragment.class.getSimpleName();

    @ViewById
    ImageView logoWhite;
    @ViewById
    RecyclerView recyclerView;
    @ViewById
    TextView descriptionTv;
    @ViewById
    Button button;

    @App
    MyApplication app;

    @FragmentArg
    User user;
    @FragmentArg
    boolean isTeacher;

    @Bean
    UserConfig userConfig;

    private IdiomSelectAdapter adapter;
    private ApiService apiService;

    @AfterViews
    public void init() {
        ViewCompat.setTransitionName(logoWhite, "logo");

        apiService = ApiFactory.getApiService();

        descriptionTv.setText(isTeacher ? R.string.which_idioms_you_teach : R.string.which_idioms_you_learn);
        button.setText(isTeacher ? R.string.next : R.string.start);

        initRecyclerView();
    }

    private void initRecyclerView() {
        GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(glm);
        recyclerView.setHasFixedSize(true);

        adapter = new IdiomSelectAdapter(null, false, true);
        recyclerView.setAdapter(adapter);
    }

    @Click
    void buttonClicked() {
        if (user.idioms == null) {
            user.idioms = new UserIdioms();
        }

        List<UserIdiom> selectedIdiomsList = adapter.getUserIdiomList();

        if (isTeacher) {
            user.idioms.teach = selectedIdiomsList;
            openSelectIdiomFragment(false, user);
        } else {
            user.idioms.learn = selectedIdiomsList;
            app.makeLogin(getActivity(), user);
            saveUserAndLoginUser();
        }

        userConfig.saveUser(user);
    }

    void openSelectIdiomFragment(Boolean isTeacher, User user) {
        if (getActivity() instanceof BaseActivity) {
            SelectIdiomFragment selectIdiom = SelectIdiomFragment_.builder().user(user).isTeacher(isTeacher).build();
            ((BaseActivity) getActivity()).switchToFragment(selectIdiom);
        }
    }

    void saveUserAndLoginUser() {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage(getResources().getString(R.string.saving_user));
        dialog.setCancelable(false);
        dialog.show();

        Gson gson = new Gson();
        String json = gson.toJson(user);

        Call<ApiResponse> loginCall = apiService.updateUser(user.token, json);
        loginCall.enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                ViewUtils.dismissDialog(dialog);

                if (apiResponse.user != null) {
                    app.makeLogin(getActivity(), user);
                } else {
                    onFailure(call, new Throwable("ApiResponse user is null"));
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                ViewUtils.dismissDialog(dialog);
                String message = errorEnum.getErrorDescription(getActivity());
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}