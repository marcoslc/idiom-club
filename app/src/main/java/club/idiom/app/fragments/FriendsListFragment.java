package club.idiom.app.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import club.idiom.app.R;
import club.idiom.app.adapters.FriendsListAdapter;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ApiService;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.models.events.GotFriendListEvent;
import club.idiom.app.singletons.UserConfig;
import retrofit2.Call;

/**
 * Created by Victor on 23/03/2016.
 */
@EFragment(R.layout.fragment_friends_list)
public class FriendsListFragment extends Fragment {
    public static final String LOGTAG = FriendsListFragment.class.getSimpleName();
    public static final String FRIENDS_FILTER = "following";
    public static final String ALL_FILTER = "all";

    @ViewById
    RecyclerView recycler;

    @ViewById
    SwipeRefreshLayout swipeRefresh;

    @Bean
    public UserConfig userConfig;

    @FragmentArg
    public String FILTER = ALL_FILTER;

    @FragmentArg
    TypeListFollow typeList;

    public ApiService apiService;

    FriendsListAdapter adapter;

    class GotUserListCallback extends IdiomApiCalback {
        public void onGetApiResponse(Call<ApiResponse> call, ApiResponse apiResponse) {
        }

        @Override
        public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
            setSwipeRefreshRefreshing(false);
            onGetApiResponse(call, apiResponse);
        }

        @Override
        public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
            Log.e(LOGTAG, "listFollowing failed: " + errorEnum);
            setSwipeRefreshRefreshing(false);

        }
    }


    @UiThread
    void setSwipeRefreshRefreshing(boolean isRefreshing) {
        if (swipeRefresh != null)
            swipeRefresh.setRefreshing(isRefreshing);

    }

    @AfterInject
    void afterInject() {
        apiService = ApiFactory.getApiService();
    }

    @AfterViews
    void init() {
        adapter = new FriendsListAdapter();

        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerView.ItemAnimator animator = recycler.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        recycler.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                boolean getOnlyFriends = FRIENDS_FILTER.equals(FILTER);
                getFriends(false, getOnlyFriends);
            }
        });

        boolean getOnlyFriends = FRIENDS_FILTER.equals(FILTER);
        getFriends(false, getOnlyFriends);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    void getFriends(boolean withDialog, boolean getOnlyFriends) {
        swipeRefresh.setRefreshing(true);
        String token = userConfig.getUser().token;

        if (getOnlyFriends) {
            switch (typeList) {
                case FOLLOWING:
                    Call<ApiResponse> listUsersCall = apiService.listFollowing(userConfig.getUser().token);
                    listUsersCall.enqueue(new GotUserListCallback() {
                        @Override
                        public void onGetApiResponse(Call<ApiResponse> call, ApiResponse apiResponse) {
                            super.onGetApiResponse(call, apiResponse);
                            if (apiResponse.following != null) {
                                adapter.add(apiResponse.following);
                                EventBus.getDefault().post(new GotFriendListEvent(apiResponse.following));
                            } else {
                                onFailure(call, new Throwable("ApiResponse user list is null"));
                            }
                        }
                    });
                    break;

                case FOLLOWERS:
                    Call<ApiResponse> listFollowersCall = apiService.listFollowers(userConfig.getUser().token);
                    listFollowersCall.enqueue(new GotUserListCallback() {
                        @Override
                        public void onGetApiResponse(Call<ApiResponse> call, ApiResponse apiResponse) {
                            super.onGetApiResponse(call, apiResponse);
                            if (apiResponse.followers != null) {
                                adapter.add(apiResponse.followers);
                                EventBus.getDefault().post(new GotFriendListEvent(apiResponse.followers));
                            } else {
                                onFailure(call, new Throwable("ApiResponse user list is null"));
                            }
                        }
                    });
                    break;

                case BLOCKED:
                    Call<ApiResponse> listBlockedCall = apiService.listFollowing(userConfig.getUser().token);
                    listBlockedCall.enqueue(new GotUserListCallback() {
                        @Override
                        public void onGetApiResponse(Call<ApiResponse> call, ApiResponse apiResponse) {
                            super.onGetApiResponse(call, apiResponse);
                            if (apiResponse.following != null) {
                                adapter.add(apiResponse.following);
                                EventBus.getDefault().post(new GotFriendListEvent(apiResponse.following));
                            } else {
                                onFailure(call, new Throwable("ApiResponse user list is null"));
                            }
                        }
                    });
                    break;
            }

        } else {
            final Call<ApiResponse> usersCall = apiService.listUsers(token);
            usersCall.enqueue(new GotUserListCallback() {
                @Override
                public void onGetApiResponse(Call<ApiResponse> call, ApiResponse apiResponse) {
                    super.onGetApiResponse(call, apiResponse);
                    if (apiResponse.users != null) {
                        adapter.add(apiResponse.users);
                    } else {
                        onFailure(call, new Throwable("ApiResponse user list is null"));
                    }
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void gotOnlineUsersList(CometChatEvent.GotOnlineListEvent event) {
        boolean getOnlyFriends = FRIENDS_FILTER.equals(FILTER);
        if (isAdded()) {
            //     getFriends(false, getOnlyFriends);
        }
    }

    public enum TypeListFollow {
        FOLLOWING, FOLLOWERS, BLOCKED;
    }
}