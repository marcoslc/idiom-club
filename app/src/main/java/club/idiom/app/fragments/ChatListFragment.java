package club.idiom.app.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import club.idiom.app.R;
import club.idiom.app.adapters.ChatsListAdapter;
import club.idiom.app.models.events.CometChatEvent;
import club.idiom.app.singletons.LastChatsInfo;

/**
 * Created by Victor on 28/04/2016.
 */
@EFragment(R.layout.fragment_friends_list)
public class ChatListFragment extends Fragment {
    public static final String LOGTAG = ChatListFragment.class.getSimpleName();

    @Bean
    LastChatsInfo lastChatsInfo;

    @ViewById
    RecyclerView recycler;

    ChatsListAdapter adapter;

    @AfterViews
    void init() {
        adapter = new ChatsListAdapter();

        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        recycler.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.setChatList(lastChatsInfo.getChatsList());
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onMessageEvent(CometChatEvent.OnMessageReceivedEvent messageReceived) {
        refreshWithDelay();
    }

    @Background(delay = 100)
    public void refreshWithDelay() {
        refresh();
    }

    @UiThread
    public void refresh() {
        adapter.setChatList(lastChatsInfo.getChatsList());
    }
}