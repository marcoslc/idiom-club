package club.idiom.app.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;

import club.idiom.app.R;
import club.idiom.app.activities.BaseActivity;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiService;
import club.idiom.app.view.SmoothViewPager;

@EFragment(R.layout.fragment_start)
public class StartFragment extends Fragment {
    public static String LOGTAG = StartFragment.class.getSimpleName();

    @ViewById
    Button loginBt, signupBt;
    @ViewById
    ImageView logoWhite;
    @ViewById
    SmoothViewPager myViewPager;
    @ViewById
    CirclePageIndicator circleIndicator;

    @StringArrayRes
    String[] carouselStringsInitial, carouselTitlesInitial;

    ApiService apiService;

    @AfterInject
    void afterInject() {
        apiService = ApiFactory.getApiService();
    }

    @AfterViews
    public void init() {
        ViewCompat.setTransitionName(logoWhite, "logo");
        initCarousel();
    }

    private void initCarousel() {
        final MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getActivity(), carouselStringsInitial, carouselTitlesInitial);
        myViewPager.setAdapter(myPagerAdapter);

        circleIndicator.setViewPager(myViewPager);
    }

    @Click
    void loginBtClicked() {
        if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) getActivity();
            activity.switchToFragmentWithSharedElementTransition(LoginFragment_.builder().build(), logoWhite, "logo");
        }
    }

    @Click
    void signupBtClicked() {
        if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) getActivity();
            activity.switchToFragmentWithSharedElementTransition(SignupFragment_.builder().build(), logoWhite, "logo");
        }
    }

    private class MyPagerAdapter extends PagerAdapter {
        String[] titles;
        String[] texts;
        Context context;

        public MyPagerAdapter(Context context, String[] texts, String[] titles) {
            this.context = context;
            this.texts = texts;
            this.titles = titles;
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.start_carousel_page, container, false);

            TextView titleTv = (TextView) view.findViewById(R.id.title);
            TextView descriptionTv = (TextView) view.findViewById(R.id.description);

            titleTv.setText(titles[position]);
            descriptionTv.setText(Html.fromHtml(texts[position]));

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

}