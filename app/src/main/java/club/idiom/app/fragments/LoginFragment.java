package club.idiom.app.fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import club.idiom.app.R;
import club.idiom.app.activities.LoginActivity;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ApiService;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.application.MyApplication;
import club.idiom.app.models.User;
import club.idiom.app.utils.InvalidFormDataException;
import club.idiom.app.utils.Utils;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

@EFragment(R.layout.fragment_login)
public class LoginFragment extends Fragment {
    public static String LOGTAG = LoginFragment.class.getSimpleName();

    @App
    MyApplication app;

    @ViewById
    EditText emailEt, passwordEt;
    @ViewById
    TextView forgetPasswordEt;
    @ViewById
    ImageView logoWhite;
    @ViewById
    Button loginBt;

    ProgressDialog loginDialog;
    ApiService apiService;

    @AfterInject
    void afterInject() {
        apiService = ApiFactory.getApiService();
    }

    @AfterViews
    public void init() {
        ViewCompat.setTransitionName(logoWhite, "logo");

        loginDialog = new ProgressDialog(getContext());
        loginDialog.setIndeterminate(true);
        loginDialog.setCancelable(false);
        loginDialog.setMessage(getString(R.string.doing_login));

        // todo erase this. just to test.
//        emailEt.setText("romvitoi@gmail.com");
//        passwordEt.setText("123123");
    }

    @Click
    void loginBtClicked() {
        String userString = emailEt.getText().toString();
        String passwordString = passwordEt.getText().toString();

        try {
            Utils.validateEmail(getActivity(), userString);
            Utils.validatePassword(getActivity(), passwordString);

            tryLogin(userString, passwordString);
        } catch (InvalidFormDataException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void tryLogin(String email, String password) {
        loginDialog.show();

        final Call<ApiResponse> loginCall = apiService.doLogin(email, password, null);
        loginCall.enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                ViewUtils.dismissDialog(loginDialog);

                User user = apiResponse.user;

                if (user != null) {
                    app.makeLogin(getActivity(), user);
                } else {
                    onFailure(call, new Throwable("ApiResponse user is null"));
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                ViewUtils.dismissDialog(loginDialog);
                String message = errorEnum.getErrorDescription(getContext());
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Click
    void enterWithFbBtClicked() {
        if (getActivity() instanceof LoginActivity) {
            ((LoginActivity) getActivity()).loginFacebook(logoWhite, "logo");
        }
    }

    @Click
    void forgetPasswordEtClicked() {

        if (getActivity() instanceof LoginActivity) {
            LoginActivity activity = (LoginActivity) getActivity();
            activity.switchToFragmentWithSharedElementTransition(ForgetPasswordFragment_.builder().build(), logoWhite, "logo");
        }

    }
}