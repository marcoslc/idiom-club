package club.idiom.app.fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import club.idiom.app.R;
import club.idiom.app.activities.BaseActivity;
import club.idiom.app.activities.LoginActivity;
import club.idiom.app.api.ApiFactory;
import club.idiom.app.api.ApiResponse;
import club.idiom.app.api.ApiService;
import club.idiom.app.api.ErrorEnum;
import club.idiom.app.api.IdiomApiCalback;
import club.idiom.app.application.MyApplication;
import club.idiom.app.models.User;
import club.idiom.app.utils.InvalidFormDataException;
import club.idiom.app.utils.Utils;
import club.idiom.app.utils.ViewUtils;
import retrofit2.Call;

@EFragment(R.layout.fragment_signup)
public class SignupFragment extends Fragment {
    public static String LOGTAG = SignupFragment.class.getSimpleName();

    @App
    MyApplication app;

    @ViewById
    EditText usernameEt, emailEt, passwordEt, passwordConfirmEt, birthdayEt, locationEt;
    @ViewById
    ImageView logoWhite;
    @ViewById
    Button loginBt;
    @ViewById
    RadioGroup genderRadioGroup;

    // Pass an user instance to prefill the form. Optional.
    @FragmentArg
    User user;

    private ApiService apiService;
    private ProgressDialog signupDialog;
    private SimpleDateFormat sdf;
    private long bornLong = 0;

    @AfterViews
    public void init() {
        ViewCompat.setTransitionName(logoWhite, "logo");

        apiService = ApiFactory.getApiService();
        sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        genderRadioGroup.clearCheck();
        genderRadioGroup.check(R.id.male_radio);
        populateFieldsWithUserData();

        signupDialog = new ProgressDialog(getContext());
        signupDialog.setIndeterminate(true);
        signupDialog.setCancelable(false);
        signupDialog.setMessage(getString(R.string.doing_signup));

        birthdayEt.setOnFocusChangeListener(datePickListener);
    }

    public void setUser(User modelUser) {
        // Update user
        user = modelUser;
        // Update screen with user data
        populateFieldsWithUserData();
    }

    @UiThread(propagation = UiThread.Propagation.REUSE)
    public void populateFieldsWithUserData() {
        if (user == null) {
            return;
        }

        if (user.born > 0) {
            // set calendar with date
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(user.born * 1000);

            // parse date and fill edit text
            String date = sdf.format(calendar.getTime());
            birthdayEt.setText(date);
        }

        usernameEt.setText(user.name);
        emailEt.setText(user.email);
        locationEt.setText(user.location);

        genderRadioGroup.clearCheck();
        genderRadioGroup.check("f".equals(user.sex) ? R.id.female_radio : R.id.male_radio);
    }

    @Click
    void signupBtClicked() {
        String userString = usernameEt.getText().toString();
        String mailString = emailEt.getText().toString();
        String passwordString = passwordEt.getText().toString();
        String passwordConfirmString = passwordConfirmEt.getText().toString();
        String location = locationEt.getText().toString();

        try {
            Utils.validateName(getActivity(), userString);
            Utils.validateEmail(getActivity(), mailString);
            Utils.validateBothPasswords(getActivity(), passwordString, passwordConfirmString);

            User newUser = new User();
            newUser.timezone = TimeZone.getDefault().getID();
            newUser.location = location;
            newUser.name = userString;
            newUser.email = mailString;
            newUser.password = passwordString;
            newUser.born = bornLong;

            try {
                Date date = sdf.parse(birthdayEt.getText().toString());
                newUser.born = date.getTime() / 1000;
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (user != null) {
                newUser.facebook = user.facebook;
            }

            newUser.photo = TextUtils.isEmpty(newUser.facebook) ? "" : "http://graph.facebook.com/" + newUser.facebook + "/picture?type=large";

            switch (genderRadioGroup.getCheckedRadioButtonId()) {
                case R.id.male_radio:
                    newUser.sex = "m";
                    break;
                case R.id.female_radio:
                    newUser.sex = "f";
                    break;
            }

            trySignup(newUser);
        } catch (InvalidFormDataException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void trySignup(User user) {
        String userAsString = new Gson().toJson(user);
        signupDialog.show();

        final Call<ApiResponse> signupCall = apiService.saveUser(userAsString);
        signupCall.enqueue(new IdiomApiCalback() {
            @Override
            public void onSuccess(Call<ApiResponse> call, ApiResponse apiResponse) {
                ViewUtils.dismissDialog(signupDialog);

                User user = apiResponse.user;

                if (user != null) {

                    if (user.needRegisterIdiom()) {
                        SelectIdiomFragment frag = SelectIdiomFragment_.builder().user(user).isTeacher(true).build();
                        ((BaseActivity) getActivity()).switchToFragmentWithSharedElementTransition(frag, logoWhite, "logo");
                    } else {
                        app.makeLogin(getActivity(), user);
                    }

                } else {
                    onFailure(call, new Throwable("ApiResponse user is null"));
                }
            }

            @Override
            public void onFail(Call<ApiResponse> call, ErrorEnum errorEnum) {
                ViewUtils.dismissDialog(signupDialog);
                String message = errorEnum.getErrorDescription(getContext());
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Click
    void enterWithFbBtClicked() {
        if (getActivity() instanceof LoginActivity) {
            ((LoginActivity) getActivity()).loginFacebook(logoWhite, "logo");
        }
    }

    private View.OnFocusChangeListener datePickListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                birthdayEt.clearFocus();

                Calendar calendar = Calendar.getInstance();
                if (bornLong > 0) {
                    calendar.setTime(new Date(bornLong));
                }

                int year = calendar.get(Calendar.YEAR);
                final int month = calendar.get(Calendar.MONTH);
                final int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog.OnDateSetListener datelistener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        bornLong = calendar.getTime().getTime();

                        birthdayEt.setText(sdf.format(calendar.getTime()));
                    }
                };

                DatePickerDialog dialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, datelistener, year, month, day);
                dialog.show();
            }
        }
    };

}